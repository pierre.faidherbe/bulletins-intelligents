import 'package:bulletins_intelligents_web_support/Screens/Classes/View/details_eleve_view.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

import 'classe.dart';
import 'competence.dart';
import 'evaluation.dart';

part 'matiere.g.dart';

@JsonSerializable(explicitToJson: true)
class Matiere extends Equatable {
  final String id;
  final String name;
  final Classe classe;

  Matiere({this.id, this.name, @required this.classe});

  Matiere copyWith(
      {String id,
      String name,
      Competence competence,
      DateTime date,
      Classe classe}) {
    return Matiere(
      id: id ?? this.id,
      name: name ?? this.name,
      classe: classe ?? this.classe,
    );
  }



  factory Matiere.fromJson(Map<String, dynamic> json) =>
      _$MatiereFromJson(json);
  Map<String, dynamic> toJson() => _$MatiereToJson(this);

  @override
  List<Object> get props => [id, name, classe];
}
