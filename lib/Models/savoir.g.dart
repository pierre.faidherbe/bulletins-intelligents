// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'savoir.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Savoir _$SavoirFromJson(Map<String, dynamic> json) {
  return Savoir(
    id: json['id'] as String,
    name: json['name'] as String,
    competence: json['competence'] == null
        ? null
        : Competence.fromJson(json['competence'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$SavoirToJson(Savoir instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'competence': instance.competence?.toJson(),
    };
