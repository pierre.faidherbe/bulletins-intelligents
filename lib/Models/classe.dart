import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

import 'eleve.dart';

part 'classe.g.dart';

@JsonSerializable(explicitToJson: true)
class Classe extends Equatable {
  final String id;
  final String name;
  final String typeOfEvaluations;

  Classe({this.id, this.name, this.typeOfEvaluations});

  Classe copyWith({String id, String name, String typeOfEvaluations}) {
    return Classe(
        id: id ?? this.id,
        name: name ?? this.name,
        typeOfEvaluations: typeOfEvaluations ?? this.typeOfEvaluations);
  }

  factory Classe.fromJson(Map<String, dynamic> json) => _$ClasseFromJson(json);
  Map<String, dynamic> toJson() => _$ClasseToJson(this);

  @override
  List<Object> get props => [id, name, typeOfEvaluations];
}
