// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'classe.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Classe _$ClasseFromJson(Map<String, dynamic> json) {
  return Classe(
    id: json['id'] as String,
    name: json['name'] as String,
    typeOfEvaluations: json['typeOfEvaluations'] as String,
  );
}

Map<String, dynamic> _$ClasseToJson(Classe instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'typeOfEvaluations': instance.typeOfEvaluations,
    };
