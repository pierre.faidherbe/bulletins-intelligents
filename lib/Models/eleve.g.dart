// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'eleve.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Eleve _$EleveFromJson(Map<String, dynamic> json) {
  return Eleve(
    id: json['id'] as String,
    prenom: json['prenom'] as String,
    nom: json['nom'] as String,
    classe: json['classe'] == null
        ? null
        : Classe.fromJson(json['classe'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$EleveToJson(Eleve instance) => <String, dynamic>{
      'id': instance.id,
      'prenom': instance.prenom,
      'nom': instance.nom,
      'classe': instance.classe?.toJson(),
    };
