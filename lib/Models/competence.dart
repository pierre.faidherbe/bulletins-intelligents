import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

import 'evaluation.dart';
import 'matiere.dart';

part 'competence.g.dart';

@JsonSerializable(explicitToJson: true)
class Competence extends Equatable {
  final String id;
  final String name;
  final Matiere matiere;

  Competence({this.id, @required this.name, @required this.matiere});

  Competence copyWith({String id, String name, Matiere matiere}) {
    return Competence(
        id: id ?? this.id,
        name: name ?? this.name,
        matiere: matiere ?? this.matiere);
  }

  factory Competence.fromJson(Map<String, dynamic> json) =>
      _$CompetenceFromJson(json);
  Map<String, dynamic> toJson() => _$CompetenceToJson(this);

  String getMoyenne(List<Evaluation> evaluations) {
    var sum = 0;
    var missingCote = 0;

    missingCote = evaluations
        .where((element) =>
            element.savoir.competence.id == this.id && element.cote == null)
        .length;

    final evaToCount = evaluations
        .where((element) =>
            element.savoir.competence.id == this.id && element.cote != null)
        .toList();

    evaToCount
        .where((element) => element.savoir.competence.id == this.id)
        .forEach((element) {
      sum += element.cote;
    });

    var result = '${(sum / evaToCount.length) * 5}% de moyenne en $name';

    if (missingCote > 0) result += ' ($missingCote cote(s) manquante(s))';

    return result;
  }

  @override
  List<Object> get props => [id, name, matiere];
}
