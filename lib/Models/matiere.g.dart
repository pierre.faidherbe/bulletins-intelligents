// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'matiere.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Matiere _$MatiereFromJson(Map<String, dynamic> json) {
  return Matiere(
    id: json['id'] as String,
    name: json['name'] as String,
    classe: json['classe'] == null
        ? null
        : Classe.fromJson(json['classe'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$MatiereToJson(Matiere instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'classe': instance.classe?.toJson(),
    };
