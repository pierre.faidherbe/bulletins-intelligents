import 'package:bulletins_intelligents_web_support/Models/savoir.dart';
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

import 'eleve.dart';

part 'evaluation.g.dart';

@JsonSerializable(explicitToJson: true)
class Evaluation extends Equatable {
  final String id;
  final Eleve eleve;
  final DateTime date;
  final String acquis;
  final num cote;
  final String commentaire;
  final Savoir savoir;
  final bool absent;

  const Evaluation(
      {this.id,
      @required this.eleve,
      @required this.date,
      this.acquis,
      this.cote,
      this.commentaire,
      this.absent = false,
      @required this.savoir});

  Evaluation copyWith(
      {String id,
      Eleve eleve,
      DateTime date,
      String acquis,
      num cote,
      String commentaire,
      Savoir savoir,
      bool absent,
      }) {
    return Evaluation(
      id: id ?? this.id,
      eleve: eleve ?? this.eleve,
      date: date ?? this.date,
      acquis: acquis ?? this.acquis,
      cote: cote ?? this.cote,
      commentaire: commentaire ?? this.commentaire,
      savoir: savoir ?? this.savoir,
      absent: absent ?? this.absent,
    );
  }

  factory Evaluation.fromJson(Map<String, dynamic> json) =>
      _$EvaluationFromJson(json);
  Map<String, dynamic> toJson() => _$EvaluationToJson(this);

  @override
  String toString() => '${eleve.fullName}, $acquis, $cote, $commentaire $absent';

  @override
  List<Object> get props =>
      [id, eleve, date, acquis, cote, commentaire, savoir, absent,];
}
