// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'evaluation.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Evaluation _$EvaluationFromJson(Map<String, dynamic> json) {
  return Evaluation(
    id: json['id'] as String,
    eleve: json['eleve'] == null
        ? null
        : Eleve.fromJson(json['eleve'] as Map<String, dynamic>),
    date: json['date'] == null ? null : DateTime.parse(json['date'] as String),
    acquis: json['acquis'] as String,
    cote: json['cote'] as num,
    commentaire: json['commentaire'] as String,
    absent: json['absent'] as bool,
    savoir: json['savoir'] == null
        ? null
        : Savoir.fromJson(json['savoir'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$EvaluationToJson(Evaluation instance) =>
    <String, dynamic>{
      'id': instance.id,
      'eleve': instance.eleve?.toJson(),
      'date': instance.date?.toIso8601String(),
      'acquis': instance.acquis,
      'cote': instance.cote,
      'commentaire': instance.commentaire,
      'savoir': instance.savoir?.toJson(),
      'absent': instance.absent,
    };
