import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

import 'evaluation.dart';
import 'classe.dart';
import 'matiere.dart';

part 'eleve.g.dart';

@JsonSerializable(explicitToJson: true)
class Eleve extends Equatable {
  final String id;
  final String prenom;
  final String nom;
  final Classe classe;

  String get fullName => '$nom $prenom';

  Eleve({this.id, this.prenom, this.nom, this.classe});

/*   Evaluation getAcquisForMatiere(Matiere _matiere) {
    return this.acquis?.firstWhere(
        (element) => element.matiere.id == _matiere.id,
        orElse: () => null);
  } */

  Eleve copyWith(
      {String id,
      String prenom,
      String nom,
      Classe classe,
      }) {
    return Eleve(
        id: id ?? this.id,
        prenom: prenom ?? this.prenom,
        nom: nom ?? this.nom,
        classe: classe ?? this.classe,
        );
  }

  factory Eleve.fromJson(Map<String, dynamic> json) => _$EleveFromJson(json);
  Map<String, dynamic> toJson() => _$EleveToJson(this);

  @override
  List<Object> get props => [id, prenom, nom, classe];
}
