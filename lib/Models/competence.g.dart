// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'competence.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Competence _$CompetenceFromJson(Map<String, dynamic> json) {
  return Competence(
    id: json['id'] as String,
    name: json['name'] as String,
    matiere: json['matiere'] == null
        ? null
        : Matiere.fromJson(json['matiere'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$CompetenceToJson(Competence instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'matiere': instance.matiere?.toJson(),
    };
