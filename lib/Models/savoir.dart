import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

import 'competence.dart';

part 'savoir.g.dart';

@JsonSerializable(explicitToJson: true)
class Savoir extends Equatable {
  final String id;
  final String name;
  final Competence competence;

  Savoir({this.id, this.name, this.competence});

  Savoir copyWith({String id, String name, Competence competence}) {
    return Savoir(
        id: id ?? this.id,
        name: name ?? this.name,
        competence: competence ?? this.competence);
  }

  factory Savoir.fromJson(Map<String, dynamic> json) => _$SavoirFromJson(json);
  Map<String, dynamic> toJson() => _$SavoirToJson(this);

  @override
  List<Object> get props => [id, name, competence];
}
