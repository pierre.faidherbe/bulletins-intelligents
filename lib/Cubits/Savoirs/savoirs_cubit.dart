import 'package:bloc/bloc.dart';
import 'package:bulletins_intelligents_web_support/Models/evaluation.dart';
import 'package:bulletins_intelligents_web_support/Models/savoir.dart';
import 'package:bulletins_intelligents_web_support/Repositories/savoirs_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'savoirs_state.dart';

class SavoirsCubit extends Cubit<SavoirsState> {
  SavoirsCubit({@required this.savoirsRepository})
      : super(SavoirsLoading()) {
    listenToSavoirs();
  }

  final SavoirsRepository savoirsRepository;

  void listenToSavoirs() {
    print('listening savoirs');
    savoirsRepository.listenToSavoirs().listen((event) {
      emit(SavoirsLoaded(event));
    });
  }

  void addSavoir(Savoir savoir) => savoirsRepository.addSavoir(savoir);

  void deleteSavoir(Savoir savoir) => savoirsRepository.deleteSavoir(savoir);

  void updateSavoir(Savoir savoir) => savoirsRepository.updateSavoir(savoir);
}
