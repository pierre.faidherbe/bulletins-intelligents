part of 'savoirs_cubit.dart';

abstract class SavoirsState extends Equatable {
  const SavoirsState();

  @override
  List<Object> get props => [];
}

class SavoirsLoading extends SavoirsState {}

class SavoirsLoaded extends SavoirsState {
  final List<Savoir> savoirs;

  SavoirsLoaded(this.savoirs);

  @override
  List<Object> get props => [savoirs];
}
