import 'package:bloc/bloc.dart';
import 'package:bulletins_intelligents_web_support/Models/classe.dart';
import 'package:bulletins_intelligents_web_support/Models/eleve.dart';
import 'package:bulletins_intelligents_web_support/Repositories/classe_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'classes_state.dart';

class ClassesCubit extends Cubit<ClassesState> {
  ClassesCubit({@required this.classesRepository})
      : super(ClassesLoading()) {
    listenToClasses();
  }

  final ClassesRepository classesRepository;



  void listenToClasses() {
    print('listening classes');
    classesRepository.listenToClasses().listen((event) {
      emit(ClassesLoaded(event));
    });
  }

  void addClasse(Classe classe) => classesRepository.addClasse(classe);

  void deleteClasse(Classe classe) => classesRepository.deleteClasse(classe);

  void updateClasse(Classe classe) => classesRepository.updateClasse(classe);
}
