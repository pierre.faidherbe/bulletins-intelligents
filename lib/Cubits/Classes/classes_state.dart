part of 'classes_cubit.dart';

abstract class ClassesState extends Equatable {
  const ClassesState();

  @override
  List<Object> get props => [];
}

class ClassesLoading extends ClassesState {}

class ClassesLoaded extends ClassesState {
  final List<Classe> classes;

  ClassesLoaded(this.classes);

  @override
  List<Object> get props => [classes];
}
