import 'package:bloc/bloc.dart';
import 'package:bulletins_intelligents_web_support/Models/competence.dart';
import 'package:bulletins_intelligents_web_support/Repositories/competences_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'competences_state.dart';

class CompetencesCubit extends Cubit<CompetencesState> {
  CompetencesCubit({@required this.competencesRepository})
      : super(CompetencesLoading()) {
    listenToCompetences();
  }

  final CompetencesRepository competencesRepository;

  void listenToCompetences() {
    print('listening competences');
    competencesRepository.listenToCompetences().listen((event) {
      emit(CompetencesLoaded(event));
    });
  }

  Future<void> addCompetence(Competence competence) => competencesRepository.addCompetence(competence);

  void deleteCompetence(Competence competence) => competencesRepository.deleteCompetence(competence);

  void updateCompetence(Competence competence) => competencesRepository.updateCompetence(competence);
}
