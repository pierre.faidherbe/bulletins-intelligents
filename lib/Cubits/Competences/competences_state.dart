part of 'competences_cubit.dart';

abstract class CompetencesState extends Equatable {
  const CompetencesState();

  @override
  List<Object> get props => [];
}

class CompetencesLoading extends CompetencesState {}

class CompetencesLoaded extends CompetencesState {
  final List<Competence> competences;

  CompetencesLoaded(this.competences);

  @override
  List<Object> get props => [competences];
}
