import 'package:bloc/bloc.dart';
import 'package:bulletins_intelligents_web_support/Models/matiere.dart';
import 'package:bulletins_intelligents_web_support/Repositories/matieres_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'matieres_state.dart';

class MatieresCubit extends Cubit<MatieresState> {
  MatieresCubit({@required this.matieresRepository})
      : super(MatieresLoading()) {
    listenToMatieres();
  }

  final MatieresRepository matieresRepository;

  void listenToMatieres() {
    print('listening matieres');
    matieresRepository.listenToMatieres().listen((event) {
      emit(MatieresLoaded(event));
    });
  }

  void addMatiere(Matiere matiere) =>
      matieresRepository.addMatiere(matiere);

  void updateMatiere(Matiere matiere) =>
      matieresRepository.updateMatiere(matiere);
      
  void deleteMatiere(Matiere matiere) =>
      matieresRepository.deleteMatiere(matiere);
}
