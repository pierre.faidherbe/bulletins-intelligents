part of 'matieres_cubit.dart';

abstract class MatieresState extends Equatable {
  const MatieresState();

  @override
  List<Object> get props => [];
}

class MatieresLoading extends MatieresState {}

class MatieresLoaded extends MatieresState {
  final List<Matiere> matieres;

  MatieresLoaded(this.matieres);

  @override
  List<Object> get props => [matieres];
}
