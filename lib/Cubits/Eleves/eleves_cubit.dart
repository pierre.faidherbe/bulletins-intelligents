import 'package:bloc/bloc.dart';
import 'package:bulletins_intelligents_web_support/Models/eleve.dart';
import 'package:bulletins_intelligents_web_support/Repositories/eleve_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'eleves_state.dart';

class ElevesCubit extends Cubit<ElevesState> {
  ElevesCubit({@required this.elevesRepository})
      : super(ElevesLoading()) {
    listenToEleves();
  }

  final ElevesRepository elevesRepository;

  void listenToEleves() {
    print('listening eleves');
    elevesRepository.listenToEleves().listen((event) {
      emit(ElevesLoaded(event));
    });
  }

  void addEleve(Eleve eleve) => elevesRepository.addEleve(eleve);

  void deleteEleve(Eleve eleve) => elevesRepository.deleteEleve(eleve);

  void updateEleve(Eleve eleve) => elevesRepository.updateEleve(eleve);
}
