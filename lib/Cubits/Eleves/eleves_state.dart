part of 'eleves_cubit.dart';

abstract class ElevesState extends Equatable {
  const ElevesState();

  @override
  List<Object> get props => [];
}

class ElevesLoading extends ElevesState {}

class ElevesLoaded extends ElevesState {
  final List<Eleve> eleves;

  ElevesLoaded(this.eleves);

  @override
  List<Object> get props => [eleves];
}

