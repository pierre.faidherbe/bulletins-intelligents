part of 'evaluations_cubit.dart';

abstract class EvaluationsState extends Equatable {
  const EvaluationsState();

  @override
  List<Object> get props => [];
}

class EvaluationsLoading extends EvaluationsState {}

class EvaluationsLoaded extends EvaluationsState {
  final List<Evaluation> evaluations;

  EvaluationsLoaded(this.evaluations);

  @override
  List<Object> get props => [evaluations];
}

