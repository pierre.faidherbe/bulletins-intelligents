import 'package:bloc/bloc.dart';
import 'package:bulletins_intelligents_web_support/Models/evaluation.dart';
import 'package:bulletins_intelligents_web_support/Repositories/evaluations_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'evaluations_state.dart';

class EvaluationsCubit extends Cubit<EvaluationsState> {
  EvaluationsCubit({@required this.evaluationsRepository})
      : super(EvaluationsLoading()) {
    listenToEvaluations();
  }

  final EvaluationsRepository evaluationsRepository;

  void listenToEvaluations() {
    print('listening evaluations');
    evaluationsRepository.listenToEvaluations().listen((event) {
      emit(EvaluationsLoaded(event));
    });
  }

  void addEvaluation(Evaluation evaluation) => evaluationsRepository.addEvaluation(evaluation);

  void deleteEvaluation(Evaluation evaluation) => evaluationsRepository.deleteEvaluation(evaluation);

  void updateEvaluation(Evaluation evaluation) => evaluationsRepository.updateEvaluation(evaluation);
}
