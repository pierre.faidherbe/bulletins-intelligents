import 'package:flutter/material.dart';

class AppColors {
  static List<Color> _colors = [
    Color.fromRGBO(172, 216, 170, 0.8), //vert
    Color.fromRGBO(247, 224, 141, 0.8), //jaune
    Color.fromRGBO(161, 205, 244, 0.8), //bleu
    Color.fromRGBO(250, 200, 205, 0.8), //rouge
  ];

  static Color getColor(int index) => _colors[_getIndex(index)];

  static int _getIndex(int index) {
    int returnIndex = index;

    if (returnIndex > _colors.length - 1) {
      while (returnIndex > _colors.length - 1) {
        returnIndex -= _colors.length;
      }
    }
    return returnIndex;
  }
}
