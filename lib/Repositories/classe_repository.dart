import 'package:bulletins_intelligents_web_support/Models/classe.dart';
import 'package:bulletins_intelligents_web_support/Models/eleve.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart' as firebase_auth;

class ClassesRepository {
  var dbClasses = FirebaseFirestore.instance;

  Stream<List<Classe>> listenToClasses() {
    String userID = firebase_auth.FirebaseAuth.instance.currentUser.uid;
    return dbClasses
        .collection('Users/$userID/Classes')
        .orderBy('name')
        .snapshots()
        .map(
            (snap) => snap.docs.map((e) => Classe.fromJson(e.data())).toList());
  }

  void addClasse(Classe classe) {
    String userID = firebase_auth.FirebaseAuth.instance.currentUser.uid;

    dbClasses
        .collection('Users/$userID/Classes')
        .add(classe.toJson())
        .then((value) => value.update(
              {'id': value.id},
            ));
  }

  void addEleve(Eleve eleve, String classeID) {
    String userID = firebase_auth.FirebaseAuth.instance.currentUser.uid;

    dbClasses
        .collection('Users/$userID/Classes/$classeID/Eleves')
        .add(eleve.toJson())
        .then((value) => value.update(
              {'id': value.id},
            ));
  }

  void deleteClasse(Classe classe) {
    String userID = firebase_auth.FirebaseAuth.instance.currentUser.uid;

    dbClasses.collection('Users/$userID/Classes').doc(classe.id).delete();
  }

  void updateClasse(Classe classe) {
    String userID = firebase_auth.FirebaseAuth.instance.currentUser.uid;

    dbClasses
        .collection('Users/$userID/Classes')
        .doc(classe.id)
        .set(classe.toJson());
  }
}
