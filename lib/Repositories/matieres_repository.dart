import 'package:bulletins_intelligents_web_support/Models/matiere.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart' as firebase_auth;

class MatieresRepository {
  var dbMatieres = FirebaseFirestore.instance;

  Stream<List<Matiere>> listenToMatieres() {
    String userID = firebase_auth.FirebaseAuth.instance.currentUser.uid;

    return dbMatieres
        .collection('Users/$userID/Matieres')
        .orderBy('name')
        .snapshots()
        .map((snap) =>
            snap.docs.map((e) => Matiere.fromJson(e.data())).toList());
  }

  void addMatiere(Matiere matiere) {
    String userID = firebase_auth.FirebaseAuth.instance.currentUser.uid;

    dbMatieres
        .collection('Users/$userID/Matieres')
        .add(matiere.toJson())
        .then((value) => value.update(
              {'id': value.id},
            ));
  }

  void deleteMatiere(Matiere matiere) {
    String userID = firebase_auth.FirebaseAuth.instance.currentUser.uid;

    dbMatieres..collection('Users/$userID/Matieres').doc(matiere.id).delete();
  }

  void updateMatiere(Matiere matiere) {
    String userID = firebase_auth.FirebaseAuth.instance.currentUser.uid;

    dbMatieres
        .collection('Users/$userID/Matieres')
        .doc(matiere.id)
        .set(matiere.toJson());
  }
}
