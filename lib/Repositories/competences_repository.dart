import 'package:bulletins_intelligents_web_support/Models/competence.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart' as firebase_auth;

class CompetencesRepository {
  var dbCompetences = FirebaseFirestore.instance;

  Stream<List<Competence>> listenToCompetences() {
    String userID = firebase_auth.FirebaseAuth.instance.currentUser.uid;

    return dbCompetences
        .collection('Users/$userID/Competences')
        .orderBy('name')
        .snapshots()
        .map((snap) =>
            snap.docs.map((e) => Competence.fromJson(e.data())).toList());
  }

  Future<void> addCompetence(Competence competence) {
    String userID = firebase_auth.FirebaseAuth.instance.currentUser.uid;

    return dbCompetences
        .collection('Users/$userID/Competences')
        .add(competence.toJson())
        .then((value) => value.update(
              {'id': value.id},
            ));
  }

  void deleteCompetence(Competence competence) {
    String userID = firebase_auth.FirebaseAuth.instance.currentUser.uid;

    dbCompetences
        .collection('Users/$userID/Competences')
        .doc(competence.id)
        .delete();
  }

  void updateCompetence(Competence competence) {
    String userID = firebase_auth.FirebaseAuth.instance.currentUser.uid;

    dbCompetences
        .collection('Users/$userID/Competences')
        .doc(competence.id)
        .set(competence.toJson());
  }
}
