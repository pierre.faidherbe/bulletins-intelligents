import 'package:bulletins_intelligents_web_support/Models/evaluation.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart' as firebase_auth;

class EvaluationsRepository {
  var dbEvaluations = FirebaseFirestore.instance;

  Stream<List<Evaluation>> listenToEvaluations() {
    String userID = firebase_auth.FirebaseAuth.instance.currentUser.uid;

    return dbEvaluations
        .collection('Users/$userID/Evaluations')
        .orderBy('eleve.nom')
        .snapshots()
        .map((snap) =>
            snap.docs.map((e) => Evaluation.fromJson(e.data())).toList());
  }

  void addEvaluation(Evaluation evaluation) {
    String userID = firebase_auth.FirebaseAuth.instance.currentUser.uid;

    dbEvaluations
        .collection('Users/$userID/Evaluations')
        .add(evaluation.toJson())
        .then((value) => value.update(
              {'id': value.id},
            ));
  }

  void deleteEvaluation(Evaluation evaluation) =>
      dbEvaluations.doc(evaluation.id).delete();

  Future<void> updateEvaluation(Evaluation evaluation) {
    String userID = firebase_auth.FirebaseAuth.instance.currentUser.uid;

    return dbEvaluations
        .collection('Users/$userID/Evaluations')
        .doc(evaluation.id)
        .set(evaluation.toJson());
  }
}
