import 'package:bulletins_intelligents_web_support/Models/eleve.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart' as firebase_auth;

class ElevesRepository {
  var dbEleves = FirebaseFirestore.instance;

  Stream<List<Eleve>> listenToEleves() {
    String userID = firebase_auth.FirebaseAuth.instance.currentUser.uid;

    return dbEleves
        .collection('Users/$userID/Eleves')
        .orderBy('nom')
        .snapshots()
        .map((snap) => snap.docs.map((e) => Eleve.fromJson(e.data())).toList());
  }

  void addEleve(Eleve eleve) {
    String userID = firebase_auth.FirebaseAuth.instance.currentUser.uid;

    dbEleves
        .collection('Users/$userID/Eleves')
        .add(eleve.toJson())
        .then((value) => value.update(
              {'id': value.id},
            ));
  }

  Future<void> deleteEleve(Eleve eleve) {
    String userID = firebase_auth.FirebaseAuth.instance.currentUser.uid;

    return dbEleves.collection('Users/$userID/Eleves').doc(eleve.id).delete();
  }

  Future<void> updateEleve(Eleve eleve) {
    String userID = firebase_auth.FirebaseAuth.instance.currentUser.uid;

    return dbEleves
        .collection('Users/$userID/Eleves')
        .doc(eleve.id)
        .set(eleve.toJson());
  }
}
