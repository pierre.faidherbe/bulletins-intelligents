import 'package:bulletins_intelligents_web_support/Models/evaluation.dart';
import 'package:bulletins_intelligents_web_support/Models/savoir.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart' as firebase_auth;

class SavoirsRepository {
  var dbSavoirs = FirebaseFirestore.instance;

  Stream<List<Savoir>> listenToSavoirs() {
    String userID = firebase_auth.FirebaseAuth.instance.currentUser.uid;

    return dbSavoirs
        .collection('Users/$userID/Savoirs')
        .orderBy('name')
        .snapshots()
        .map(
            (snap) => snap.docs.map((e) => Savoir.fromJson(e.data())).toList());
  }

  void addSavoir(Savoir savoir) {
    String userID = firebase_auth.FirebaseAuth.instance.currentUser.uid;

    dbSavoirs.collection('Users/$userID/Savoirs').add(savoir.toJson()).then(
      (value) {
        value.update(
          {'id': value.id},
        );
      },
    );
  }

  void deleteSavoir(Savoir savoir) {
    String userID = firebase_auth.FirebaseAuth.instance.currentUser.uid;

    dbSavoirs.collection('Users/$userID/Savoirs').doc(savoir.id).delete();
  }

  void updateSavoir(Savoir savoir) {
    String userID = firebase_auth.FirebaseAuth.instance.currentUser.uid;

    dbSavoirs
        .collection('Users/$userID/Savoirs')
        .doc(savoir.id)
        .set(savoir.toJson());
  }
}
