import 'package:bulletins_intelligents_web_support/Cubits/Eleves/eleves_cubit.dart';
import 'package:bulletins_intelligents_web_support/Cubits/Evaluations/evaluations_cubit.dart';
import 'package:bulletins_intelligents_web_support/Models/competence.dart';
import 'package:bulletins_intelligents_web_support/Models/eleve.dart';
import 'package:bulletins_intelligents_web_support/Models/evaluation.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/Prompts/alert_eleve.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/View/eleves_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import '../../../colors.dart';

class DetailsEleveView extends StatefulWidget {
  const DetailsEleveView(
      {Key key, @required this.eleve, @required this.competence})
      : super(key: key);

  final Eleve eleve;
  final Competence competence;
  static const routeName = '/classes/classe/eleves/eleve';

  @override
  _DetailsEleveViewState createState() => _DetailsEleveViewState();
}

Eleve eleve;
List<Evaluation> evaluations;

Color getColorEva(String eva) {
  if (eva == 'A.')
    return Colors.green;
  else if (eva == 'E.V.A.')
    return Colors.orange;
  else if (eva == 'N.A.') return Colors.red;
}

class _DetailsEleveViewState extends State<DetailsEleveView> {
  String moyenne() => widget.competence.getMoyenne(evaluations);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ElevesCubit, ElevesState>(
      builder: (context, state) {
        if (state is ElevesLoaded)
          eleve = state.eleves
              .firstWhere((element) => element.id == widget.eleve.id);
        return BlocBuilder<EvaluationsCubit, EvaluationsState>(
          builder: (context, state) {
            if (state is EvaluationsLoading)
              return Center(
                child: CircularProgressIndicator(),
              );
            else if (state is EvaluationsLoaded)
              evaluations = state.evaluations
                  .where((element) => element.eleve.id == eleve.id)
                  .toList();
            return Scaffold(
              appBar: AppBar(
                backgroundColor: AppColors.getColor(0),
                centerTitle: true,
                title: Text('${eleve.fullName}'),
                actions: [
                  widget.eleve.classe.typeOfEvaluations == 'Cotes'
                      ? Center(
                          child: Text(
                          moyenne(), //TODO: moyenne if absent
                          style: TextStyle(color: Colors.black),
                        ))
                      : Container(),
                  IconButton(
                    icon: Icon(Icons.delete_outline),
                    onPressed: () => showDialog(
                      context: context,
                      builder: (context) => AlertDialogDeleteEleve(
                        eleve: eleve,
                      ),
                    ),
                  ),
                  IconButton(
                    icon: Icon(Icons.edit_outlined),
                    onPressed: () => showDialog(
                      context: context,
                      builder: (context) => AlertDialogEleve(
                        classe: eleve.classe,
                        eleve: eleve,
                      ),
                    ),
                  ),
                ],
              ),
              body: BlocBuilder<EvaluationsCubit, EvaluationsState>(
                builder: (context, state) {
                  if (state is EvaluationsLoading)
                    return Center(child: CircularProgressIndicator());
                  else if (state is EvaluationsLoaded)
                    evaluations = state.evaluations
                        .where((element) =>
                            element.eleve.id == eleve.id &&
                            element.savoir.competence.id ==
                                widget.competence.id)
                        .toList();
                  return Container(
                    width: double.infinity,
                    child: DataTable(
                      columns: [
                        DataColumn(
                          label: Text('Matière'),
                        ),
                        DataColumn(
                          label: Text('Compétence'),
                        ),
                        DataColumn(
                          label: Text('Savoir'),
                        ),
                        widget.eleve.classe.typeOfEvaluations == 'Acquis'
                            ? DataColumn(
                                label: Text('Acquis'),
                              )
                            : DataColumn(
                                label: Text('Cote'),
                              ),
                        DataColumn(
                          label: Text('Commentaire'),
                        ),
                        DataColumn(
                          label: Text('Date'),
                        ),
                      ],
                      rows: evaluations
                          .map(
                            (e) => DataRow(cells: [
                              DataCell(
                                Text(e.savoir.competence.matiere.name),
                              ),
                              DataCell(
                                Text(e.savoir.competence.name),
                              ),
                              DataCell(
                                Text(e.savoir.name),
                              ),
                              widget.eleve.classe.typeOfEvaluations == 'Acquis'
                                  ? DataCell(
                                      e.absent
                                          ? Text(
                                              'Absent',
                                              style: TextStyle(
                                                  fontStyle: FontStyle.italic),
                                            )
                                          : Text(
                                              e.acquis.toString() ?? '?',
                                              style: TextStyle(
                                                  color: getColorEva(e.acquis)),
                                            ),
                                    )
                                  : DataCell(
                                      e.absent
                                          ? Text(
                                              'Absent',
                                              style: TextStyle(
                                                  fontStyle: FontStyle.italic),
                                            )
                                          : Text(e.cote.toString() ?? '?'),
                                    ),
                              DataCell(
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width / 6,
                                    child: Text(
                                      e.commentaire.replaceAll('\n', ' ') ?? '',
                                      softWrap: true,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                  onTap: () => e.commentaire.isNotEmpty
                                      ? showDialog(
                                          context: context,
                                          builder: (context) => AlertDialog(
                                            content: Text(e.commentaire),
                                          ),
                                        )
                                      : null),
                              DataCell(
                                Text(
                                  DateFormat.yMMMMd().format(e.date),
                                ),
                              ),
                            ]),
                          )
                          .toList(),
                    ),
                  );
                },
              ),
            );
          },
        );
      },
    );
  }
}

class AlertDialogDeleteEleve extends StatelessWidget {
  const AlertDialogDeleteEleve({
    Key key,
    @required this.eleve,
  }) : super(key: key);

  final Eleve eleve;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Supprimer l\'elève'),
      actions: [
        FlatButton(
          onPressed: () => Navigator.pop(context),
          child: Text('Annuler'),
        ),
        FlatButton(
          onPressed: () {
            context.bloc<ElevesCubit>().deleteEleve(eleve);
            Navigator.popUntil(
                context, ModalRoute.withName(ElevesView.routeName));
          },
          child: Text(
            'Effacer',
            style: TextStyle(color: Colors.red),
          ),
        ),
      ],
      content: Text(
          'Voulez-vous supprimer cette elève ? Cela effacera tout son contenu'),
    );
  }
}
