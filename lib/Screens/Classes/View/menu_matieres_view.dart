import 'package:bulletins_intelligents_web_support/Cubits/Matieres/matieres_cubit.dart';
import 'package:bulletins_intelligents_web_support/Models/classe.dart';
import 'package:bulletins_intelligents_web_support/Models/competence.dart';
import 'package:bulletins_intelligents_web_support/Models/matiere.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/Prompts/alert_matiere.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/View/savoirs_view.dart';
import 'package:bulletins_intelligents_web_support/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'competence_view.dart';

class MenuMatieresView extends StatefulWidget {
  const MenuMatieresView({Key key, @required this.classe}) : super(key: key);
  final Classe classe;

  static const routeName = '/classes/classe/matieres';

  @override
  _MenuMatieresViewState createState() => _MenuMatieresViewState();
}

class _MenuMatieresViewState extends State<MenuMatieresView> {
  List<Matiere> matieres;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: Image.asset('Icons/matieres.png'),
      appBar: AppBar(
        backgroundColor: AppColors.getColor(1),
        title: Text('Mes matières'),
      ),
      body: BlocBuilder<MatieresCubit, MatieresState>(
        builder: (context, state) {
          if (state is MatieresLoading)
            return Center(
              child: CircularProgressIndicator(),
            );
          else if (state is MatieresLoaded)
            matieres = state.matieres
                .where((element) => element.classe.id == widget.classe.id)
                .toList();
          return Center(
            child: Container(
              child: buildDe(matieres.length, matieres),
            ),
          );
        },
      ),
    );
  }

  Container buildDe(int nbOfElement, List<Matiere> matieres) {
    if (nbOfElement == 0)
      return buildCircleItemMatiere(index: nbOfElement);
    else if (nbOfElement == 1)
      return Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            buildCircleItemMatiere(matiere: matieres[0], index: 0),
            buildCircleItemMatiere(index: nbOfElement)
          ],
        ),
      );
    else if (nbOfElement == 2)
      return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemMatiere(matiere: matieres[0], index: 0),
                buildCircleItemMatiere(matiere: matieres[1], index: 1)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [buildCircleItemMatiere(index: nbOfElement)],
            )
          ],
        ),
      );
    else if (nbOfElement == 3)
      return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemMatiere(matiere: matieres[0], index: 0),
                buildCircleItemMatiere(matiere: matieres[1], index: 1)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemMatiere(matiere: matieres[2], index: 2),
                buildCircleItemMatiere(index: nbOfElement),
              ],
            )
          ],
        ),
      );
    else if (nbOfElement == 4)
      return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemMatiere(matiere: matieres[0], index: 0, size: 4),
                buildCircleItemMatiere(matiere: matieres[1], index: 1, size: 4)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemMatiere(matiere: matieres[2], index: 2, size: 4),
                buildCircleItemMatiere(matiere: matieres[3], index: 3, size: 4),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemMatiere(size: 4, index: nbOfElement),
                Container(
                  width: MediaQuery.of(context).size.width / 4,
                )
              ],
            )
          ],
        ),
      );
    else if (nbOfElement == 5)
      return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemMatiere(matiere: matieres[0], index: 0, size: 4),
                buildCircleItemMatiere(matiere: matieres[1], index: 1, size: 4)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemMatiere(matiere: matieres[2], index: 2, size: 4),
                buildCircleItemMatiere(matiere: matieres[3], index: 3, size: 4),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemMatiere(matiere: matieres[4], index: 4, size: 4),
                buildCircleItemMatiere(size: 4, index: nbOfElement),
              ],
            )
          ],
        ),
      );
    else if (nbOfElement == 6)
      return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemMatiere(matiere: matieres[0], index: 0, size: 4),
                buildCircleItemMatiere(matiere: matieres[1], index: 1, size: 4)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemMatiere(matiere: matieres[2], index: 2, size: 4),
                buildCircleItemMatiere(matiere: matieres[3], index: 3, size: 4),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemMatiere(matiere: matieres[4], index: 4, size: 4),
                buildCircleItemMatiere(matiere: matieres[5], index: 5, size: 4),
              ],
            )
          ],
        ),
      );
    return Container();
  }

  Container buildCircleItemMatiere(
      {Matiere matiere, int size = 3, @required int index}) {
    return Container(
      height: MediaQuery.of(context).size.height / size,
      width: MediaQuery.of(context).size.width / size,
      child: Material(
        color: AppColors.getColor(index),
        type: MaterialType.circle,
        child: InkWell(
          onTap: () {
            matiere != null
                ? Navigator.push(
                    context,
                    CupertinoPageRoute(
                      settings: RouteSettings(name:CompetenceView.routeName),
                      builder: (context) => CompetenceView(
                        
                        matiere: matiere,
                      ),
                    ),
                  )
                : showDialog(
                    context: context,
                    builder: (context) => AlertDialogMatiere(
                      classe: widget.classe,
                    ),
                  );
            ;
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ListTile(
                title: Text(
                  matiere?.name ?? 'Ajouter une matière',
                  textAlign: TextAlign.center,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}



