import 'package:bulletins_intelligents_web_support/Cubits/Classes/classes_cubit.dart';
import 'package:bulletins_intelligents_web_support/Models/classe.dart';
import 'package:bulletins_intelligents_web_support/Repositories/classe_repository.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/Prompts/alert_classe.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/View/details_classe_view.dart';
import 'package:bulletins_intelligents_web_support/bloc/authentication_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../colors.dart';

class MenuClassesView extends StatefulWidget {
  const MenuClassesView({Key key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<void>(
        builder: (_) => MenuClassesView(),
        settings: RouteSettings(name: '/classes'));
  }

  @override
  _MenuClassesViewState createState() => _MenuClassesViewState();
}

class _MenuClassesViewState extends State<MenuClassesView> {
  Classe choosenClasse;
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Mes Classes'),
        actions: [IconButton(icon: Icon(Icons.logout), onPressed: () => context
                .bloc<AuthenticationBloc>()
                .add(AuthenticationLogoutRequested()),
          )],
      ),
      //drawer: MyDrawer(),
      body: Container(
        child: BlocBuilder<ClassesCubit, ClassesState>(
          builder: (context, state) {
            if (state is ClassesLoading)
              return Center(
                child: CircularProgressIndicator(),
              );
            else if (state is ClassesLoaded)
              return Center(
                child: Container(
                  child: buildDe(state.classes.length, state.classes),
                ),
              );
          },
        ),
      ),
    );
  }

  Container buildDe(int nbOfElement, List<Classe> classes) {
    if (nbOfElement == 0)
      return buildCircleItemClasse(index: nbOfElement);
    else if (nbOfElement == 1)
      return Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            buildCircleItemClasse(classe: classes[0], index: 0),
            buildCircleItemClasse(index: nbOfElement)
          ],
        ),
      );
    else if (nbOfElement == 2)
      return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemClasse(classe: classes[0], index: 0),
                buildCircleItemClasse(classe: classes[1], index: 1)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                buildCircleItemClasse(index: nbOfElement),
              ],
            )
          ],
        ),
      );
    else if (nbOfElement == 3)
      return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemClasse(classe: classes[0], index: 0),
                buildCircleItemClasse(classe: classes[1], index: 1)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemClasse(classe: classes[2], index: 2),
                buildCircleItemClasse(index: nbOfElement),
              ],
            )
          ],
        ),
      );
    else if (nbOfElement == 4)
      return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemClasse(classe: classes[0], index: 0, size: 4),
                buildCircleItemClasse(classe: classes[1], index: 1, size: 4)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemClasse(classe: classes[2], index: 2, size: 4),
                buildCircleItemClasse(classe: classes[3], index: 3, size: 4),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemClasse(size: 4, index: nbOfElement),
                Container(
                  width: MediaQuery.of(context).size.width / 4,
                )
              ],
            )
          ],
        ),
      );
    else if (nbOfElement == 5)
      return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemClasse(classe: classes[0], index: 0, size: 4),
                buildCircleItemClasse(classe: classes[1], index: 1, size: 4)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemClasse(classe: classes[2], index: 2, size: 4),
                buildCircleItemClasse(classe: classes[3], index: 3, size: 4),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemClasse(classe: classes[4], index: 4, size: 4),
                buildCircleItemClasse(size: 4, index: nbOfElement),
              ],
            )
          ],
        ),
      );
    else if (nbOfElement == 6)
      return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemClasse(classe: classes[0], index: 0, size: 4),
                buildCircleItemClasse(classe: classes[1], index: 1, size: 4)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemClasse(classe: classes[2], index: 2, size: 4),
                buildCircleItemClasse(classe: classes[3], index: 3, size: 4),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemClasse(classe: classes[4], index: 4, size: 4),
                buildCircleItemClasse(classe: classes[5], index: 5, size: 4),
              ],
            )
          ],
        ),
      );
    return Container();
  }

  Container buildCircleItemClasse({
    Classe classe,
    int size = 3,
    @required int index,
  }) {
    return Container(
      height: MediaQuery.of(context).size.height / size,
      width: MediaQuery.of(context).size.width / size,
      child: Material(
        color: AppColors.getColor(index),
        type: MaterialType.circle,
        child: InkWell(
          onTap: () {
            classe == null
                ? showDialog(
                    context: context,
                    builder: (context) => AlertDialogClasse(
                      classe: classe,
                    ),
                  )
                : Navigator.push(
                    context,
                    MaterialPageRoute(
                      settings: RouteSettings(name: '/classes/classe'),
                      builder: (context) => DetailsClasseView(
                        classe: classe,
                      ),
                    ));
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ListTile(
                title: Text(
                  classe?.name ?? 'Ajouter une classe',
                  textAlign: TextAlign.center,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
