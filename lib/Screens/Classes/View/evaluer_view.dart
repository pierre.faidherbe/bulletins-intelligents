import 'package:bulletins_intelligents_web_support/Cubits/Eleves/eleves_cubit.dart';
import 'package:bulletins_intelligents_web_support/Cubits/Evaluations/evaluations_cubit.dart';
import 'package:bulletins_intelligents_web_support/Cubits/Matieres/matieres_cubit.dart';
import 'package:bulletins_intelligents_web_support/Cubits/Savoirs/savoirs_cubit.dart';
import 'package:bulletins_intelligents_web_support/Models/classe.dart';
import 'package:bulletins_intelligents_web_support/Models/eleve.dart';
import 'package:bulletins_intelligents_web_support/Models/evaluation.dart';
import 'package:bulletins_intelligents_web_support/Models/matiere.dart';
import 'package:bulletins_intelligents_web_support/Models/savoir.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/Prompts/carousel_evaluations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../colors.dart';

class EvaluerView extends StatefulWidget {
  const EvaluerView({Key key, @required this.classe}) : super(key: key);
  final Classe classe;
  static const routeName = '/classes/classe/evaluer';

  @override
  _EvaluerViewState createState() => _EvaluerViewState();
}

class _EvaluerViewState extends State<EvaluerView> {
  List<Matiere> matieres = [];
  List<Savoir> savoirs = [];
  List<Eleve> eleves = [];
  List<Evaluation> evaluations = [];
  Savoir selectedSavoir;
  DateTime selectedDate;
  final globalKey = GlobalKey<ScaffoldState>();

  Future<DateTime> _showDialog() {
    return showDatePicker(
      locale: Locale('fr', 'FR'),
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime.now().subtract(Duration(days: 365)),
      lastDate: DateTime.now().add(
        Duration(days: 365),
      ),
    );
  }

  void evaluate() {
    showDialog(
      context: context,
      builder: (context) => CarouselEvaluations(
        classe: widget.classe,
        eleves: eleves
            .where((element) => !evaluations.any((_element) {
                  return widget.classe.typeOfEvaluations == 'Acquis'
                      ? _element.eleve.id == element.id &&
                          _element.acquis !=
                              null && //TODO: passer les bons élèves
                          _element.savoir.id == selectedSavoir.id
                      : _element.eleve.id == element.id &&
                          _element.cote !=
                              null && //TODO: passer les bons élèves
                          _element.savoir.id == selectedSavoir.id;
                }))
            .toList(),
        date: selectedDate,
        savoir: selectedSavoir,
        showSnack: () => globalKey.currentState.showSnackBar(
          SnackBar(
            content: Text('Evaluations ajoutées !'),
          ),
        ),
      ),
    );
  }

  bool checkIfEvaIsFinish(
      List<Evaluation> evaluations, List<Eleve> eleves, Savoir savoir) {
    bool check;

    if (widget.classe.typeOfEvaluations == 'Acquis') {
      check = eleves.length ==
          evaluations
              .where((element) =>
                  element.savoir.id == savoir.id && element.acquis != null)
              .length;
    } else {
      check = eleves.length ==
          evaluations
              .where((element) =>
                  element.savoir.id == savoir.id && element.cote != null)
              .length;
    }
    return check;
  }

  String getCounterOfEva(
      List<Evaluation> evaluations, List<Eleve> eleves, Savoir savoir) {
    int nbOfEleves = eleves.length;
    int nbOfEva;

    if (widget.classe.typeOfEvaluations == 'Acquis') {
      nbOfEva = evaluations
          .where((element) =>
              element.savoir.id == savoir.id && element.acquis != null)
          .length;
    } else {
      nbOfEva = evaluations
          .where((element) =>
              element.savoir.id == savoir.id && element.cote != null)
          .length;
    }

    return '$nbOfEva/$nbOfEleves';
  }

  List<ListTile> buildSavoirs(List<Savoir> savoirs) {
    var temp = savoirs
        .map((e) => ListTile(
              trailing: checkIfEvaIsFinish(evaluations, eleves, e)
                  ? Icon(
                      Icons.check,
                      color: Colors.green,
                    )
                  : Text(getCounterOfEva(evaluations, eleves, e)),
              title: Text(e.name),
              subtitle: Text(e.competence.name),
              onTap: () {
                if (!checkIfEvaIsFinish(evaluations, eleves, e)) {
                  setState(() {
                    selectedSavoir = e;
                    _showDialog().then((value) {
                      selectedDate = value;
                      if (selectedDate != null) evaluate();
                    });
                  });
                } else
                  return null;
              },
            ))
        .toList();

    return savoirs.isEmpty
        ? [
            ListTile(
              subtitle: Text(
                'Vous n\'avez pas de savoirs dans cette matière',
                style: TextStyle(fontStyle: FontStyle.italic),
              ),
            ),
          ]
        : temp;
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MatieresCubit, MatieresState>(
      builder: (context, state) {
        if (state is MatieresLoaded)
          matieres = state.matieres
              .where((element) => element.classe.id == widget.classe.id)
              .toList();
        return BlocBuilder<SavoirsCubit, SavoirsState>(
          builder: (context, state) {
            if (state is SavoirsLoaded) savoirs = state.savoirs;
            return BlocBuilder<EvaluationsCubit, EvaluationsState>(
              builder: (context, state) {
                if (state is EvaluationsLoaded) evaluations = state.evaluations;
                return BlocBuilder<ElevesCubit, ElevesState>(
                  builder: (context, state) {
                    if (state is ElevesLoaded)
                      eleves = state.eleves
                          .where((element) =>
                              element.classe.id == widget.classe.id)
                          .toList();
                    return Scaffold(
                      key: globalKey,
                      floatingActionButton: Image.asset('Icons/noter.png'),
                      appBar: AppBar(
                        backgroundColor: AppColors.getColor(2),
                        title: Text('Evaluer'),
                      ),
                      body: Center(
                        child: Container(
                          width: MediaQuery.of(context).size.width / 3,
                          child: ListView.builder(
                            itemCount: matieres.length,
                            itemBuilder: (BuildContext context, int index) {
                              var temp = savoirs
                                  .where((element) =>
                                      element.competence.matiere.id ==
                                      matieres[index].id)
                                  .toList();
                              return ExpansionTile(
                                  title: Text(matieres[index].name),
                                  children: buildSavoirs(temp));
                            },
                          ),
                        ),
                      ),
                    );
                  },
                );
              },
            );
          },
        );
      },
    );
  }
}

class RowCircleEvaluateButton extends StatefulWidget {
  const RowCircleEvaluateButton({
    Key key,
    this.index,
    this.evaluation,
    this.field,
    @required this.enabled,
  }) : super(key: key);

  final int index;
  final Evaluation evaluation;
  final FormFieldState field;
  final bool enabled;

  @override
  _RowCircleEvaluateButtonState createState() =>
      _RowCircleEvaluateButtonState();
}

class _RowCircleEvaluateButtonState extends State<RowCircleEvaluateButton> {
  String acquisSelected;

  void onAcquisSelected(String _acquisSelected) {
    widget.field.didChange(_acquisSelected);

    setState(() {
      acquisSelected = _acquisSelected;
    });
  }

  @override
  void initState() {
    print("row: ${widget.evaluation}");
    if (widget.evaluation != null) acquisSelected = widget.evaluation.acquis;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        widget.enabled
            ? CircleEvaluateButton(
                title: 'A.',
                selectedColor: Colors.green,
                onTap: (_acquisSelected) => onAcquisSelected(_acquisSelected),
                selected: acquisSelected == 'A.',
              )
            : DisabledCircleEvaluateButton(title: 'A.'),
        widget.enabled
            ? CircleEvaluateButton(
                title: 'E.V.A.',
                selectedColor: Colors.orange,
                onTap: (_acquisSelected) => onAcquisSelected(_acquisSelected),
                selected: acquisSelected == 'E.V.A.',
              )
            : DisabledCircleEvaluateButton(title: 'E.V.A.'),
        widget.enabled
            ? CircleEvaluateButton(
                title: 'N.A.',
                selectedColor: Colors.red,
                onTap: (_acquisSelected) => onAcquisSelected(_acquisSelected),
                selected: acquisSelected == 'N.A.')
            : DisabledCircleEvaluateButton(title: 'N.A.'),
      ],
    );
  }
}

class DisabledCircleEvaluateButton extends StatelessWidget {
  const DisabledCircleEvaluateButton({Key key, this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      width: 100,
      child: Material(
        color: Colors.white,
        type: MaterialType.circle,
        child: Container(
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(color: Colors.grey),
          ),
          child: InkWell(
            child: Center(
              child: Text(
                title,
                style: TextStyle(color: Colors.grey),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class CircleEvaluateButton extends StatelessWidget {
  const CircleEvaluateButton(
      {Key key, this.selectedColor, this.title, this.selected, this.onTap})
      : super(key: key);

  final Color selectedColor;
  final String title;
  final bool selected;
  final ValueSetter<String> onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      width: 100,
      child: Material(
        color: selected ? selectedColor : Colors.white,
        type: MaterialType.circle,
        child: Container(
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(color: selectedColor),
          ),
          child: InkWell(
            onTap: () {
              return onTap(title);
            },
            child: Center(
              child: Text(
                title,
                style:
                    TextStyle(color: selected ? Colors.white : selectedColor),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
