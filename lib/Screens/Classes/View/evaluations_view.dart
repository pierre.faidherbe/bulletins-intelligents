import 'package:bulletins_intelligents_web_support/Cubits/Evaluations/evaluations_cubit.dart';
import 'package:bulletins_intelligents_web_support/Models/classe.dart';
import 'package:bulletins_intelligents_web_support/Models/evaluation.dart';
import 'package:bulletins_intelligents_web_support/Models/savoir.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/Prompts/carousel_evaluations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import '../../../colors.dart';

class EvaluationsView extends StatefulWidget {
  const EvaluationsView({Key key, @required this.classe, @required this.savoir})
      : super(key: key);
  final Classe classe;
  final Savoir savoir;
  static const routeName = '/classes/classe/evaluations';

  @override
  _EvaluationsViewState createState() => _EvaluationsViewState();
}

class _EvaluationsViewState extends State<EvaluationsView> {
  List<Evaluation> evaluations;
  bool _sortAscending = true;
  int _sortIndex;

  Color getColorEva(String eva) {
    if (eva == 'A.')
      return Colors.green;
    else if (eva == 'E.V.A.')
      return Colors.orange;
    else if (eva == 'N.A.') return Colors.red;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Evaluations'),
        backgroundColor: AppColors.getColor(3),
      ),
      body: Container(
        child: BlocBuilder<EvaluationsCubit, EvaluationsState>(
          builder: (context, state) {
            if (state is EvaluationsLoading)
              return Center(
                child: CircularProgressIndicator(),
              );
            else if (state is EvaluationsLoaded)
              evaluations = state.evaluations;

            return Scrollbar(
              child: SingleChildScrollView(
                child: Align(
                  alignment: AlignmentDirectional.topCenter,
                  child: Container(
                    width: double.infinity,
                    child: DataTable(
                      showCheckboxColumn: false,
                      sortAscending: _sortAscending,
                      sortColumnIndex: _sortIndex,
                      columns: [
                        DataColumn(
                          label: Text('Nom'),
                          onSort: (columnIndex, ascending) {
                            ascending
                                ? evaluations.sort((a, b) =>
                                    a.eleve.nom.compareTo(b.eleve.nom))
                                : evaluations.sort((a, b) =>
                                    b.eleve.nom.compareTo(a.eleve.nom));
                            _sortAscending = ascending;
                            _sortIndex = columnIndex;
                            setState(() {});
                          },
                        ),
                        DataColumn(
                          label: Text('Prénom'),
                          onSort: (columnIndex, ascending) {
                            ascending
                                ? evaluations.sort((a, b) =>
                                    a.eleve.prenom.compareTo(b.eleve.prenom))
                                : evaluations.sort((a, b) =>
                                    b.eleve.prenom.compareTo(a.eleve.prenom));
                            _sortAscending = ascending;
                            _sortIndex = columnIndex;
                            setState(() {});
                          },
                        ),
                        DataColumn(
                          label: Text('Matière'),
                        ),
                        DataColumn(
                          label: Text('Compétence'),
                        ),
                        DataColumn(
                          label: Text('Savoir'),
                        ),
                        widget.classe.typeOfEvaluations == 'Acquis'
                            ? DataColumn(
                                label: Text('Acquis'),
                              )
                            : DataColumn(
                                label: Text('Cote'),
                              ),
                        DataColumn(
                          label: Text('Commentaire'),
                        ),
                        DataColumn(
                          label: Text('Date'),
                        ),
                      ],
                      rows: evaluations
                          .where((element) =>
                              element.savoir.id == widget.savoir.id)
                          .map(
                            (e) => DataRow(
                              onSelectChanged: (bool) => showDialog(
                                context: context,
                                builder: (context) => Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                        height:
                                            MediaQuery.of(context).size.height /
                                                1.5,
                                        width:
                                            MediaQuery.of(context).size.width /
                                                1.5,
                                        child: Card(
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(35)),
                                          child: EvaluationCard(
                                              evaluation: e,
                                              savoir: e.savoir,
                                              date: e.date,
                                              eleve: e.eleve,
                                              index: 0,
                                              onEvaluationReceived: (value) {
                                                context
                                                    .bloc<EvaluationsCubit>()
                                                    .updateEvaluation(
                                                        value.values.first);
                                                Navigator.pop(context);
                                              }),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              cells: [
                                DataCell(
                                  Text(e.eleve.nom),
                                ),
                                DataCell(
                                  Text(e.eleve.prenom),
                                ),
                                DataCell(
                                  Text(e.savoir.competence.matiere.name),
                                ),
                                DataCell(
                                  Text(e.savoir.competence.name),
                                ),
                                DataCell(
                                  Text(
                                    e.savoir.name.replaceAll('\n', ' ') ?? '',
                                    softWrap: true,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                                widget.classe.typeOfEvaluations == 'Acquis'
                                    ? DataCell(
                                        e.absent
                                            ? Text(
                                                'Absent',
                                                style: TextStyle(
                                                    fontStyle:
                                                        FontStyle.italic),
                                              )
                                            : Text(
                                                e.acquis.toString() ?? '?',
                                                style: TextStyle(
                                                    color:
                                                        getColorEva(e.acquis)),
                                              ),
                                      )
                                    : DataCell(
                                        e.absent
                                            ? Text(
                                                'Absent',
                                                style: TextStyle(
                                                    fontStyle:
                                                        FontStyle.italic),
                                              )
                                            : Text(e.cote.toString() ?? '?'),
                                      ),
                                DataCell(
                                    Container(
                                      width:
                                          MediaQuery.of(context).size.width / 6,
                                      child: Text(
                                        e.commentaire.replaceAll('\n', ' ') ??
                                            '',
                                        softWrap: true,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                    onTap: () => e.commentaire.isNotEmpty
                                        ? showDialog(
                                            context: context,
                                            builder: (context) => AlertDialog(
                                              content: Text(e.commentaire),
                                            ),
                                          )
                                        : null),
                                DataCell(
                                  Text(DateFormat.yMMMd().format(e.date)),
                                ),
                              ],
                            ),
                          )
                          .toList(),
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
