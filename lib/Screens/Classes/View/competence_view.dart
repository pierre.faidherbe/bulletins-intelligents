import 'package:bulletins_intelligents_web_support/Cubits/Competences/competences_cubit.dart';
import 'package:bulletins_intelligents_web_support/Cubits/Competences/competences_cubit.dart';
import 'package:bulletins_intelligents_web_support/Cubits/Matieres/matieres_cubit.dart';
import 'package:bulletins_intelligents_web_support/Models/competence.dart';
import 'package:bulletins_intelligents_web_support/Models/competence.dart';
import 'package:bulletins_intelligents_web_support/Models/matiere.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/Prompts/alert_competence.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/Prompts/alert_matiere.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/View/menu_matieres_view.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/View/savoirs_view.dart';
import 'package:bulletins_intelligents_web_support/colors.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CompetenceView extends StatefulWidget {
  const CompetenceView({
    Key key,
    @required this.matiere,
  }) : super(key: key);

  final Matiere matiere;

  static String routeName = '/classes/classe/matieres/competences';

  @override
  _CompetenceViewState createState() => _CompetenceViewState();
}

class _CompetenceViewState extends State<CompetenceView> {
  List<Competence> competences = [];
  bool sort = true;
  Competence selectedCompetence;
  Matiere matiere;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((Duration duration) {
      FeatureDiscovery.discoverFeatures(
        context,
        const <String>{
          'comp',
        },
      );
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MatieresCubit, MatieresState>(
      builder: (context, state) {
        if (state is MatieresLoading)
          return Center(
            child: CircularProgressIndicator(),
          );
        else if (state is MatieresLoaded)
          matiere = state.matieres
              .firstWhere((element) => element.id == widget.matiere.id);
        return Scaffold(
          floatingActionButton: Image.asset('Icons/matieres.png'),
          appBar: AppBar(
            centerTitle: true,
            backgroundColor: AppColors.getColor(1),
            title: Text(
              matiere.name,
            ),
            actions: [
              IconButton(
                icon: Icon(Icons.delete_outline),
                onPressed: () => showDialog(
                  context: context,
                  builder: (context) => AlertDialogDeleteMatiere(
                    matiere: matiere,
                  ),
                ),
              ),
              IconButton(
                icon: Icon(Icons.edit_outlined),
                onPressed: () => showDialog(
                  context: context,
                  builder: (context) => AlertDialogMatiere(
                    matiere: matiere,
                  ),
                ),
              ),
            ],
          ),
          body: BlocBuilder<CompetencesCubit, CompetencesState>(
            builder: (context, state) {
              if (state is CompetencesLoading) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              } else if (state is CompetencesLoaded) {
                competences = state.competences
                    .where((element) => element.matiere.id == matiere?.id)
                    .toList();

                return Center(
                  child: DescribedFeatureOverlay(
                    featureId: 'comp',
                    backgroundColor: AppColors.getColor(1),
                    title: Text(
                        'Rajoutez ici vos compétences ! (Savoir-lire, Savoir-écrire... )'),
                    tapTarget: Icon(
                      Icons.check,
                      color: AppColors.getColor(1),
                    ),
                    child: Container(
                      child: buildDe(competences.length, competences),
                    ),
                  ),
                );
              }
            },
          ),
        );
      },
    );
  }

  Container buildDe(int nbOfElement, List<Competence> competences) {
    if (nbOfElement == 0)
      return buildCircleItemCompetence(index: nbOfElement);
    else if (nbOfElement == 1)
      return Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            buildCircleItemCompetence(competence: competences[0], index: 0),
            buildCircleItemCompetence(index: 1)
          ],
        ),
      );
    else if (nbOfElement == 2)
      return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemCompetence(competence: competences[0], index: 0),
                buildCircleItemCompetence(competence: competences[1], index: 1)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [buildCircleItemCompetence(index: nbOfElement)],
            )
          ],
        ),
      );
    else if (nbOfElement == 3)
      return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemCompetence(competence: competences[0], index: 0),
                buildCircleItemCompetence(competence: competences[1], index: 1)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemCompetence(competence: competences[2], index: 2),
                buildCircleItemCompetence(index: nbOfElement),
              ],
            )
          ],
        ),
      );
    else if (nbOfElement == 4)
      return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemCompetence(
                    competence: competences[0], index: 0, size: 4),
                buildCircleItemCompetence(
                    competence: competences[1], index: 1, size: 4)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemCompetence(
                    competence: competences[2], index: 2, size: 4),
                buildCircleItemCompetence(
                    competence: competences[3], index: 3, size: 4),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemCompetence(size: 4, index: nbOfElement),
                Container(
                  width: MediaQuery.of(context).size.width / 4,
                )
              ],
            )
          ],
        ),
      );
    else if (nbOfElement == 5)
      return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemCompetence(
                    competence: competences[0], index: 0, size: 4),
                buildCircleItemCompetence(
                    competence: competences[1], index: 1, size: 4)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemCompetence(
                    competence: competences[2], index: 2, size: 4),
                buildCircleItemCompetence(
                    competence: competences[3], index: 3, size: 4),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemCompetence(
                    competence: competences[4], index: 4, size: 4),
                buildCircleItemCompetence(size: 4, index: nbOfElement),
              ],
            )
          ],
        ),
      );
    else if (nbOfElement == 6)
      return Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemCompetence(
                    competence: competences[0], index: 0, size: 4),
                buildCircleItemCompetence(
                    competence: competences[1], index: 1, size: 4)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemCompetence(
                    competence: competences[2], index: 2, size: 4),
                buildCircleItemCompetence(
                    competence: competences[3], index: 3, size: 4),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildCircleItemCompetence(competence: competences[4], index: 4),
                buildCircleItemCompetence(competence: competences[5], index: 5),
              ],
            )
          ],
        ),
      );
    return Container();
  }

  Container buildCircleItemCompetence(
      {Competence competence, int size = 3, @required index}) {
    return Container(
      height: MediaQuery.of(context).size.height / size,
      width: MediaQuery.of(context).size.width / size,
      child: Material(
        color: AppColors.getColor(index),
        type: MaterialType.circle,
        child: InkWell(
          onTap: () {
            competence != null
                ? Navigator.push(
                    context,
                    CupertinoPageRoute(
                      settings: RouteSettings(name: SavoirsView.routeName),
                      builder: (context) => SavoirsView(
                        competence: competence,
                      ),
                    ),
                  )
                : showDialog(
                    context: context,
                    builder: (context) => AlertDialogCompetence(
                      competence: competence,
                      matiere: matiere,
                    ),
                  );
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ListTile(
                title: Text(
                  competence?.name ?? 'Ajouter une \n compétence',
                  textAlign: TextAlign.center,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class AlertDialogDeleteMatiere extends StatelessWidget {
  const AlertDialogDeleteMatiere({
    Key key,
    @required this.matiere,
  }) : super(key: key);

  final Matiere matiere;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Supprimer la matière'),
      actions: [
        FlatButton(
          onPressed: () => Navigator.pop(context),
          child: Text('Annuler'),
        ),
        FlatButton(
          onPressed: () {
            context.bloc<MatieresCubit>().deleteMatiere(matiere);
            Navigator.popUntil(
                context, ModalRoute.withName(MenuMatieresView.routeName));
          },
          child: Text(
            'Effacer',
            style: TextStyle(color: Colors.red),
          ),
        ),
      ],
      content: Text(
          'Voulez-vous supprimer cette matière ? Cela effacera tout son contenu'),
    );
  }
}
