
import 'package:bulletins_intelligents_web_support/Cubits/Competences/competences_cubit.dart';
import 'package:bulletins_intelligents_web_support/Cubits/Eleves/eleves_cubit.dart';
import 'package:bulletins_intelligents_web_support/Models/classe.dart';
import 'package:bulletins_intelligents_web_support/Models/competence.dart';
import 'package:bulletins_intelligents_web_support/Models/eleve.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/Prompts/alert_eleve.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/View/details_eleve_view.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

import '../../../colors.dart';

class ElevesView extends StatefulWidget {
  const ElevesView({Key key, this.classe}) : super(key: key);

  final Classe classe;
  static const routeName = '/classes/classe/eleves';

  @override
  _ElevesViewState createState() => _ElevesViewState();
}

class _ElevesViewState extends State<ElevesView> {
  List<Eleve> eleves = [];
  bool _sortAscending = true;
  int _sortIndex;

  void sorting(int index, bool ascending) {
    if (index == 1)
      ascending
          ? eleves.sort((a, b) => a.nom.compareTo(b.nom))
          : eleves.sort((a, b) => b.nom.compareTo(a.nom));
    else if (index == 2)
      ascending
          ? eleves.sort((a, b) => a.prenom.compareTo(b.prenom))
          : eleves.sort((a, b) => b.prenom.compareTo(a.prenom));
  }

  @override
  void initState() {
    // ...
    WidgetsBinding.instance.addPostFrameCallback((Duration duration) {
      FeatureDiscovery.discoverFeatures(
        context,
        const <String>{
          'test',
        },
      );
    });
    super.initState();
  }

  void goToEleve(Eleve eleve) {
    final GlobalKey<FormBuilderState> fbKey = GlobalKey<FormBuilderState>();

    showDialog(
      context: context,
      builder: (context) {
        List<Competence> competences;
        return BlocBuilder<CompetencesCubit, CompetencesState>(
          builder: (context, state) {
            if (state is CompetencesLoading)
              return Center(child: CircularProgressIndicator());
            else if (state is CompetencesLoaded)
              competences = state.competences
                  .where((element) =>
                      element.matiere.classe.id == widget.classe.id)
                  .toList();
            if (competences.isEmpty)
              return AlertDialog(
                  title: Text('Vous n\'avez pas encore ajouté de compétences'));
            return AlertDialog(
              title: Text('Choissisez une compétence'),
              actions: [
                FlatButton(
                  child: Text('Voir les évaluations'),
                  onPressed: () {
                    if (fbKey.currentState.saveAndValidate())
                      return Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => DetailsEleveView(
                            eleve: eleve,
                            competence: fbKey.currentState.value['competence'],
                          ),
                        ),
                      );
                  },
                )
              ],
              content: FormBuilder(
                key: fbKey,
                child: FormBuilderDropdown(
                    attribute: 'competence',
                    initialValue: competences.first,
                    items: competences
                        .map((e) => DropdownMenuItem(
                              value: e,
                              child: Text(e.name),
                            ))
                        .toList()),
              ),
            );
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: Image.asset('Icons/classe.png'),
      appBar: AppBar(
        title: Text(widget.classe.name),
        backgroundColor: AppColors.getColor(0),
      ),
      body: BlocBuilder<ElevesCubit, ElevesState>(
        builder: (context, state) {
          if (state is ElevesLoading) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is ElevesLoaded) {
            eleves = state.eleves
                .where((element) => widget.classe.id == element.classe.id)
                .toList();
            sorting(_sortIndex, _sortAscending);
            return Scrollbar(
              child: ListView(
                children: [
                  Center(
                    child: Container(
                      width: MediaQuery.of(context).size.width / 3,
                      child: Column(
                        children: [
                          DescribedFeatureOverlay(
                            featureId: 'test',
                            backgroundColor: AppColors.getColor(0),
                            title: Text(
                                'Cliquez sur un élève pour voir ses évaluations ou pour le modifier.'),
                            tapTarget: Icon(
                              Icons.check,
                              color: AppColors.getColor(0),
                            ),
                            child: DataTable(
                              sortAscending: _sortAscending,
                              sortColumnIndex: _sortIndex,
                              showCheckboxColumn: false,
                              columns: [
                                DataColumn(
                                  label: Text(
                                    '#',
                                    style: TextStyle(fontSize: 18),
                                  ),
                                ),
                                DataColumn(
                                  onSort: (columnIndex, ascending) {
                                    _sortAscending = ascending;
                                    _sortIndex = columnIndex;
                                    setState(() {});
                                  },
                                  label: Text(
                                    'Nom',
                                    style: TextStyle(fontSize: 18),
                                  ),
                                ),
                                DataColumn(
                                  label: Text(
                                    'Prénom',
                                    style: TextStyle(fontSize: 18),
                                  ),
                                  onSort: (columnIndex, ascending) {
                                    _sortAscending = ascending;
                                    _sortIndex = columnIndex;
                                    setState(() {});
                                  },
                                )
                              ],
                              rows: eleves
                                  .map(
                                    (e) => DataRow(
                                      onSelectChanged: (bool) {
                                        goToEleve(e);
                                      },
                                      cells: [
                                        DataCell(
                                          Text((eleves.indexOf(e) + 1)
                                              .toString()),
                                        ),
                                        DataCell(
                                          Text(e.nom),
                                        ),
                                        DataCell(
                                          Text(e.prenom),
                                        ),
                                      ],
                                    ),
                                  )
                                  .toList(),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: IconButton(
                              icon: Icon(Icons.add),
                              onPressed: () {
                                showDialog(
                                  context: context,
                                  builder: (context) => AlertDialogEleve(
                                    classe: widget.classe,
                                  ),
                                );
                              },
                              iconSize: 32,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            );
          }
        },
      ),
    );
  }
}
