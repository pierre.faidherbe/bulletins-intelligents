import 'package:bulletins_intelligents_web_support/Cubits/Competences/competences_cubit.dart';
import 'package:bulletins_intelligents_web_support/Cubits/Savoirs/savoirs_cubit.dart';
import 'package:bulletins_intelligents_web_support/Models/classe.dart';
import 'package:bulletins_intelligents_web_support/Models/competence.dart';
import 'package:bulletins_intelligents_web_support/Models/savoir.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/View/evaluations_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../colors.dart';

class MenuEvaluationsView extends StatefulWidget {
  const MenuEvaluationsView({Key key, this.classe}) : super(key: key);

  final Classe classe;
  static const routeName = '/classes/classe/evaluations';
  @override
  _MenuEvaluationsViewState createState() => _MenuEvaluationsViewState();
}

class _MenuEvaluationsViewState extends State<MenuEvaluationsView> {
  List<Competence> competences;
  List<Savoir> savoirs;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CompetencesCubit, CompetencesState>(
      builder: (context, state) {
        if (state is CompetencesLoading)
          return Center(
            child: CircularProgressIndicator(),
          );
        else if (state is CompetencesLoaded)
          competences = state.competences
              .where((element) => element.matiere.classe.id == widget.classe.id)
              .toList();
        return BlocBuilder<SavoirsCubit, SavoirsState>(
          builder: (context, state) {
            if (state is SavoirsLoading)
              return Center(
                child: CircularProgressIndicator(),
              );
            else if (state is SavoirsLoaded) savoirs = state.savoirs;
            return Scaffold(
              appBar: AppBar(
                title: Text('Evaluations'),
                backgroundColor: AppColors.getColor(3),
              ),
              floatingActionButton: Image.asset('Icons/evaluations.png'),
              body: Center(
                child: Container(
                  width: MediaQuery.of(context).size.width / 3,
                  child: ListView.builder(
                    itemCount: competences.length,
                    itemBuilder: (BuildContext context, int index) {
                      return ExpansionTile(
                        title: Text(competences[index].name),
                        children: savoirs
                            .where((element) =>
                                element.competence.id == competences[index].id)
                            .map(
                              (e) => ListTile(
                                title: Text(e.name),
                                onTap: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => EvaluationsView(
                                      classe: widget.classe,
                                      savoir: e,
                                    ),
                                  ),
                                ),
                              ),
                            )
                            .toList(),
                      );
                    },
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }
}
