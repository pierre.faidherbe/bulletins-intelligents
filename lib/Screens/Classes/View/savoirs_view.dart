import 'package:bulletins_intelligents_web_support/Cubits/Competences/competences_cubit.dart';
import 'package:bulletins_intelligents_web_support/Cubits/Savoirs/savoirs_cubit.dart';
import 'package:bulletins_intelligents_web_support/Models/competence.dart';
import 'package:bulletins_intelligents_web_support/Models/savoir.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/Prompts/alert_competence.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/Prompts/alert_savoir.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/View/competence_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../colors.dart';

class SavoirsView extends StatefulWidget {
  const SavoirsView({
    Key key,
    this.competence,
  }) : super(key: key);

  final Competence competence;

  static String routeName = '/classes/classe/matieres/matiere/savoirs';

  @override
  _SavoirsViewState createState() => _SavoirsViewState();
}

class _SavoirsViewState extends State<SavoirsView> {
  bool sort = true;
  int sortedIndex;
  List<Savoir> savoirs;
  Competence competence;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CompetencesCubit, CompetencesState>(
      builder: (context, state) {
        if (state is CompetencesLoaded)
          competence = state.competences
              .firstWhere((element) => element.id == widget.competence.id);
        return Scaffold(
          floatingActionButton: Image.asset('Icons/matieres.png'),
          appBar: AppBar(
            centerTitle: true,
            backgroundColor: AppColors.getColor(1),
            title: Text(competence.name),
            actions: [
              IconButton(
                icon: Icon(Icons.delete_outline),
                onPressed: () => showDialog(
                  context: context,
                  builder: (context) => AlertDialogDeleteCompetence(
                    competence: competence,
                  ),
                ),
              ),
              IconButton(
                icon: Icon(Icons.edit_outlined),
                onPressed: () => showDialog(
                  context: context,
                  builder: (context) => AlertDialogCompetence(
                    competence: competence,
                  ),
                ),
              ),
            ],
          ),
          body: BlocBuilder<SavoirsCubit, SavoirsState>(
            builder: (context, state) {
              if (state is SavoirsLoading) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              } else if (state is SavoirsLoaded) {
                savoirs = state.savoirs
                    .where((element) =>
                        element.competence.id == widget.competence?.id)
                    .toList();
                if (widget.competence == null)
                  return Container(
                      width: MediaQuery.of(context).size.width / 3,
                      child: Center(
                        child: Text('Pas de compétence selectionnée'),
                      ));
                return Scrollbar(
                  child: SingleChildScrollView(
                    child: Center(
                      child: Container(
                        width: MediaQuery.of(context).size.width / 3,
                        child: Column(
                          children: [
                            DataTable(
                              showCheckboxColumn: false,
                              sortAscending: sort,
                              sortColumnIndex: sortedIndex,
                              columns: [
                                DataColumn(
                                  onSort: (columnIndex, ascending) {
                                    setState(() {
                                      sort = !sort;
                                      sortedIndex = columnIndex;
                                    });
                                    //onSortColum(columnIndex, ascending);
                                  },
                                  label: Text(
                                    'Savoir',
                                    style: TextStyle(fontSize: 18),
                                  ),
                                ),
                              ],
                              rows: savoirs
                                  .map(
                                    (e) => DataRow(
                                      onSelectChanged: (bool) => showDialog(
                                        context: context,
                                        builder: (context) => AlertDialogSavoir(
                                          savoir: e,
                                        ),
                                      ),
                                      cells: [
                                        DataCell(
                                          Text(e.name),
                                        ),
                                        /*  DataCell(
                                      Text(e.competence.name),
                                    ),
                                    DataCell(
                                      Text(DateFormat.yMMMd().format(e.date)),
                                    ), */
                                      ],
                                    ),
                                  )
                                  .toList(),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(20.0),
                              child: IconButton(
                                icon: Icon(Icons.add),
                                onPressed: () {
                                  showDialog(
                                    context: context,
                                    builder: (context) => AlertDialogSavoir(
                                      competence: widget.competence,
                                    ),
                                  );
                                },
                                iconSize: 32,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              }
            },
          ),
        );
      },
    );
  }
}

class AlertDialogDeleteCompetence extends StatelessWidget {
  const AlertDialogDeleteCompetence({
    Key key,
    @required this.competence,
  }) : super(key: key);

  final Competence competence;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Supprimer la compétence'),
      actions: [
        FlatButton(
          onPressed: () => Navigator.pop(context),
          child: Text('Annuler'),
        ),
        FlatButton(
          onPressed: () {
            context.bloc<CompetencesCubit>().deleteCompetence(competence);
            Navigator.popUntil(
                context, ModalRoute.withName(CompetenceView.routeName));
          },
          child: Text(
            'Effacer',
            style: TextStyle(color: Colors.red),
          ),
        ),
      ],
      content: Text(
          'Voulez-vous supprimer cette compétence ? Cela effacera tout son contenu'),
    );
  }
}
