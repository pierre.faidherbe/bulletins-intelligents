import 'package:bulletins_intelligents_web_support/Cubits/Classes/classes_cubit.dart';
import 'package:bulletins_intelligents_web_support/Models/classe.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/Prompts/alert_classe.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/View/menu_classes_view.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/Widgets/circle_item_details_class_widget.dart';
import 'package:buy_me_a_coffee_widget/buy_me_a_coffee_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../colors.dart';

class DetailsClasseView extends StatefulWidget {
  const DetailsClasseView({Key key, this.classe}) : super(key: key);

  final Classe classe;

  @override
  _DetailsClasseViewState createState() => _DetailsClasseViewState();
}

class _DetailsClasseViewState extends State<DetailsClasseView> {
  Classe classe;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ClassesCubit, ClassesState>(
      builder: (context, state) {
        if (state is ClassesLoading)
          return Center(child: CircularProgressIndicator());
        else if (state is ClassesLoaded)
          classe = state.classes
              .firstWhere((element) => element.id == widget.classe.id);
        return Scaffold(
          bottomNavigationBar: BottomAppBar(
            elevation: 0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  child: IconButton(
                    onPressed: () => {
                      launch(
                          'https://www.facebook.com/Bulletins-Intelligents-175507933937415')
                    },
                    iconSize: 50,
                    icon: FaIcon(
                      FontAwesomeIcons.facebook,
                      size: 50,
                    ),
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 20.0),
                  child: InkWell(
                    onTap: () {},
                    child: BuyMeACoffeeWidget(
                      sponsorID: 'bulletins.intel',
                      theme: TealTheme(),
                    ),
                  ),
                ),
              ],
            ),
          ),
          appBar: AppBar(
            centerTitle: true,
            title: Text(classe.name),
            actions: [
              IconButton(
                icon: Icon(Icons.delete_outline),
                onPressed: () => showDialog(
                  context: context,
                  builder: (context) => AlertDialogDeleteClasse(classe: classe),
                ),
              ),
              IconButton(
                icon: Icon(Icons.edit_outlined),
                onPressed: () => showDialog(
                  context: context,
                  builder: (context) => AlertDialogClasse(
                    classe: classe,
                  ),
                ),
              ),
            ],
          ),
          body: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Stack(
                      alignment: Alignment.topCenter,
                      children: [
                        CircleItemDetailsClasse(
                          color: AppColors.getColor(0),
                          classe: classe,
                          title: 'Mes élèves',
                          routeIndex: 0,
                        ),
                        Transform.translate(
                          offset: Offset(0, -40),
                          child: Image.asset(
                            'Icons/classe.png',
                          ),
                        ),
                      ],
                    ),
                    Stack(
                      alignment: Alignment.topCenter,
                      children: [
                        CircleItemDetailsClasse(
                          classe: widget.classe,
                          color: AppColors.getColor(1),
                          title: 'Mes matières, \n compétences et savoirs',
                          routeIndex: 1,
                        ),
                        Transform.translate(
                          offset: Offset(0, -40),
                          child: Image.asset(
                            'Icons/matieres.png',
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Stack(
                      alignment: Alignment.topCenter,
                      children: [
                        CircleItemDetailsClasse(
                          classe: classe,
                          color: AppColors.getColor(2),
                          title: 'J\'évalue mes élèves',
                          routeIndex: 2,
                        ),
                        Transform.translate(
                          offset: Offset(0, -40),
                          child: Image.asset(
                            'Icons/noter.png',
                          ),
                        ),
                      ],
                    ),
                    Stack(
                      alignment: Alignment.topCenter,
                      children: [
                        CircleItemDetailsClasse(
                          classe: classe,
                          color: AppColors.getColor(3),
                          title: 'Mes évaluations',
                          routeIndex: 3,
                        ),
                        Transform.translate(
                          offset: Offset(0, -40),
                          child: Image.asset(
                            'Icons/evaluations.png',
                          ),
                        ),
                      ],
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }
}

class AlertDialogDeleteClasse extends StatelessWidget {
  const AlertDialogDeleteClasse({
    Key key,
    @required this.classe,
  }) : super(key: key);

  final Classe classe;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Supprimer la classe'),
      actions: [
        FlatButton(
          onPressed: () => Navigator.pop(context),
          child: Text('Annuler'),
        ),
        FlatButton(
          onPressed: () {
            context.bloc<ClassesCubit>().deleteClasse(classe);
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => MenuClassesView()));
          },
          child: Text(
            'Effacer',
            style: TextStyle(color: Colors.red),
          ),
        ),
      ],
      content: Text(
          'Voulez-vous supprimer cette classe ? Cela effacera tout son contenu'),
    );
  }
}
