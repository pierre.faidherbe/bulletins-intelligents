import 'package:bulletins_intelligents_web_support/Cubits/Matieres/matieres_cubit.dart';
import 'package:bulletins_intelligents_web_support/Models/classe.dart';
import 'package:bulletins_intelligents_web_support/Models/matiere.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class AlertDialogMatiere extends StatefulWidget {
  AlertDialogMatiere({Key key, this.matiere, this.classe}) : super(key: key);

  final Matiere matiere;
  final Classe classe;

  @override
  _AlertDialogMatiereState createState() => _AlertDialogMatiereState();
}

class _AlertDialogMatiereState extends State<AlertDialogMatiere> {
  Matiere matiere;
  TextEditingController nomTextEditingController = TextEditingController();
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  void addMatiere(Matiere matiere) {
    context.bloc<MatieresCubit>().addMatiere(matiere);
    Navigator.pop(context);
  }

  void updateMatiere(Matiere matiere) {
    context.bloc<MatieresCubit>().updateMatiere(matiere);
    Navigator.pop(context);
  }

  void deleteMatiere(Matiere matiere) {
    context.bloc<MatieresCubit>().deleteMatiere(matiere);
    Navigator.pop(context);
  }

  @override
  void initState() {
    super.initState();
    matiere = widget.matiere;
    nomTextEditingController.text = matiere?.name;
  }

  void submit() {
    if (_fbKey.currentState.saveAndValidate())
      matiere == null
          ? addMatiere(
              Matiere(
                name: nomTextEditingController.text,
                classe: widget.classe,
              ),
            )
          : updateMatiere(
              matiere.copyWith(
                name: nomTextEditingController.text,
                classe: widget.classe,
              ),
            );
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: matiere == null
          ? Text('Ajouter une matière')
          : Text('Modifier la matière'),
      actions: [
        FlatButton(
          onPressed: () => Navigator.pop(context),
          child: Text('Annuler'),
        ),
        FlatButton(
          onPressed: () => submit(),
          child: matiere == null ? Text('Ajouter') : Text('Modifier'),
        ),
        
      ],
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          FormBuilder(
            key: _fbKey,
            child: FormBuilderTextField(
              
              attribute: 'nom',
              autofocus: true,
              controller: nomTextEditingController,
              decoration: InputDecoration(labelText: 'Nom'),
              validators: [
                FormBuilderValidators.minLength(1,
                    errorText: 'Entrez un nom pour la matière')
              ],
              onFieldSubmitted: (value) => submit(),
            ),
          ),
        ],
      ),
    );
  }
}
