import 'package:bulletins_intelligents_web_support/Cubits/Savoirs/savoirs_cubit.dart';
import 'package:bulletins_intelligents_web_support/Models/competence.dart';
import 'package:bulletins_intelligents_web_support/Models/savoir.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class AlertDialogSavoir extends StatefulWidget {
  AlertDialogSavoir({Key key, this.savoir, this.competence}) : super(key: key);

  final Savoir savoir;
  final Competence competence;

  @override
  _AlertDialogSavoirState createState() => _AlertDialogSavoirState();
}

class _AlertDialogSavoirState extends State<AlertDialogSavoir> {
  Savoir savoir;
  TextEditingController nomTextEditingController = TextEditingController();
  Competence competence;
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  void addSavoir(Savoir savoir) {
    context.bloc<SavoirsCubit>().addSavoir(savoir);
    Navigator.pop(context);
  }

  void updateSavoir(Savoir savoir) {
    context.bloc<SavoirsCubit>().updateSavoir(savoir);
    Navigator.pop(context);
  }

  void deleteSavoir(Savoir savoir) {
    context.bloc<SavoirsCubit>().deleteSavoir(savoir);
    Navigator.pop(context);
  }

  @override
  void initState() {
    super.initState();
    savoir = widget.savoir;
    competence = widget.competence;
    nomTextEditingController.text = savoir?.name;
  }

  void submit() {
    if (_fbKey.currentState.saveAndValidate())
      savoir == null
          ? addSavoir(
              Savoir(
                name: nomTextEditingController.text,
                competence: competence,
              ),
            )
          : updateSavoir(
              savoir.copyWith(
                name: nomTextEditingController.text,
                //competence: competence ?? competences.first,
              ),
            );
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: savoir == null
          ? Text('Ajouter un savoir')
          : Text('Modifier le savoir'),
      actions: [
        if (savoir != null)
          FlatButton(
            onPressed: () {
              deleteSavoir(savoir);
            },
            child: Text('Effacer'),
          ),
        FlatButton(
          onPressed: () => submit(),
          child: savoir == null ? Text('Ajouter') : Text('Modifier'),
        ),
      ],
      content: FormBuilder(
        key: _fbKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            FormBuilderTextField(
              autofocus: true,
              attribute: 'nom',
              controller: nomTextEditingController,
              decoration: InputDecoration(labelText: 'Nom'),
              validators: [
                FormBuilderValidators.minLength(1,
                    errorText: 'Entrez un nom pour le savoir')
              ],
              onFieldSubmitted: (value) => submit(),
            ),
          ],
        ),
      ),
    );
  }
}
