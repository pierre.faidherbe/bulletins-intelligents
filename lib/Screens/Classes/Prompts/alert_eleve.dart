import 'package:bulletins_intelligents_web_support/Cubits/Classes/classes_cubit.dart';
import 'package:bulletins_intelligents_web_support/Cubits/Eleves/eleves_cubit.dart';
import 'package:bulletins_intelligents_web_support/Models/classe.dart';
import 'package:bulletins_intelligents_web_support/Models/eleve.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class AlertDialogEleve extends StatefulWidget {
  AlertDialogEleve({Key key, this.eleve, @required this.classe})
      : super(key: key);

  final Eleve eleve;
  final Classe classe;

  @override
  _AlertDialogEleveState createState() => _AlertDialogEleveState();
}

class _AlertDialogEleveState extends State<AlertDialogEleve> {
  Eleve eleve;
  TextEditingController nomTextEditingController = TextEditingController();
  TextEditingController prenomTextEditingController = TextEditingController();
  Classe classe;
  List<Classe> classes;
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  void addEleve(Eleve eleve) {
    context.bloc<ElevesCubit>().addEleve(eleve);
    Navigator.pop(context);
  }

  void updateEleve(Eleve eleve) {
    context.bloc<ElevesCubit>().updateEleve(eleve);
    Navigator.pop(context);
  }

  @override
  void initState() {
    super.initState();
    eleve = widget.eleve;
    classe = widget.classe;
    nomTextEditingController.text = eleve?.nom;
    prenomTextEditingController.text = eleve?.prenom;
  }

  void submit() {
    if (_fbKey.currentState.saveAndValidate())
      eleve == null
          ? addEleve(
              Eleve(
                  nom: nomTextEditingController.text,
                  prenom: prenomTextEditingController.text,
                  classe: classe),
            )
          : updateEleve(
              eleve.copyWith(
                  nom: _fbKey.currentState.value['nom'],
                  prenom: _fbKey.currentState.value['prenom'],
                  classe: _fbKey.currentState.value['classe']),
            );
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      
      actions: [
        
        FlatButton(
          child: Text('Annuler'),
          onPressed: () => Navigator.pop(context),
        ),
        FlatButton(
          onPressed: () => submit(),
          child: eleve == null ? Text('Ajouter') : Text('Modifier'),
        )
      ],
      content: BlocBuilder<ClassesCubit, ClassesState>(
        builder: (context, state) {
          if (state is ClassesLoading)
            return Center(
              child: CircularProgressIndicator(),
            );
          else if (state is ClassesLoaded) classes = state.classes;
          return FormBuilder(
            key: _fbKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                FormBuilderTextField(
                  attribute: 'nom',
                  controller: nomTextEditingController,
                  decoration: InputDecoration(labelText: 'Nom'),
                  autofocus: true,
                  validators: [
                    FormBuilderValidators.minLength(1,
                        errorText: 'Entrez un nom pour l\'élève')
                  ],
                  onFieldSubmitted: (value) => submit(),
                ),
                FormBuilderTextField(
                  attribute: 'prenom',
                  controller: prenomTextEditingController,
                  decoration: InputDecoration(labelText: 'Prénom '),
                  validators: [
                    FormBuilderValidators.minLength(1,
                        errorText: 'Entrez un prénom pour l\'élève')
                  ],
                  onFieldSubmitted: (value) => submit(),
                ),
                if(eleve != null) Padding(
                  padding: const EdgeInsets.symmetric(vertical:8.0),
                  child: FormBuilderDropdown(
                    attribute: 'classe',
                    items: classes.map(
                      (e) => DropdownMenuItem(
                        child: Text(e.name),
                        value: e,
                      ),
                    ).toList(),
                    initialValue: widget.classe,
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
