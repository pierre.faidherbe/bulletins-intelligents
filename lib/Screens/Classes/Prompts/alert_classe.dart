import 'package:bulletins_intelligents_web_support/Cubits/Classes/classes_cubit.dart';
import 'package:bulletins_intelligents_web_support/Models/classe.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class AlertDialogClasse extends StatefulWidget {
  AlertDialogClasse({Key key, this.classe}) : super(key: key);

  final Classe classe;

  @override
  _AlertDialogClasseState createState() => _AlertDialogClasseState();
}

class _AlertDialogClasseState extends State<AlertDialogClasse> {
  List<Classe> classes;

  Classe classe;

  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  void addClasse(Classe classe) {
    context.bloc<ClassesCubit>().addClasse(classe);
    Navigator.pop(context);
  }

  void updateClasse(Classe classe) {
    context.bloc<ClassesCubit>().updateClasse(classe);
    Navigator.pop(context);
  }

  @override
  void initState() {
    super.initState();
    classe = widget.classe;
  }

  void submit() {
    if (_fbKey.currentState.saveAndValidate())
      classe == null
          ? addClasse(
              Classe(
                  name: _fbKey.currentState.value['name'],
                  typeOfEvaluations:
                      _fbKey.currentState.value['evaluationType']),
            )
          : updateClasse(
              classe.copyWith(
                name: _fbKey.currentState.value['name'],
                /* typeOfEvaluations:
                      _fbKey.currentState.value['evaluationType'] */
              ),
            );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ClassesCubit, ClassesState>(
      builder: (context, state) {
        if (state is ClassesLoading) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else if (state is ClassesLoaded) {
          classes = state.classes;
          return AlertDialog(
            title: classe == null
                ? Text('Ajouter une classe')
                : Text('Modifier une classe'),
            actions: [
              FlatButton(
                child: Text('Annuler'),
                onPressed: () => Navigator.pop(context),
              ),
              FlatButton(
                onPressed: () => submit(),
                child: classe == null ? Text('Ajouter') : Text('Modifier'),
              )
            ],
            content: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                FormBuilder(
                  key: _fbKey,
                  child: Column(
                    children: [
                      FormBuilderTextField(
                        initialValue: classe?.name,
                        attribute: 'name',
                        decoration: InputDecoration(labelText: 'Nom'),
                        autofocus: true,
                        validators: [
                          FormBuilderValidators.minLength(1,
                              errorText: 'Entrez un nom pour la classe')
                        ],
                        onFieldSubmitted: (value) => submit(),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      if (classe == null)
                        FormBuilderRadioGroup(
                          initialValue: classe?.typeOfEvaluations,
                          attribute: 'evaluationType',
                          orientation: GroupedRadioOrientation.wrap,
                          wrapSpacing: 20,
                          validators: [
                            FormBuilderValidators.required(
                                errorText:
                                    'Vous devez choisir un type d\'évaluation')
                          ],
                          decoration: InputDecoration(
                            labelText: 'Type d\'évaluations',
                            helperText:
                                '\n Acquis:  A. / E.V.A. / N.A. \n\n Cote: Note sur 20 par exemple',
                          ),
                          options: [
                            FormBuilderFieldOption(value: 'Acquis'),
                            FormBuilderFieldOption(value: 'Cotes'),
                          ],
                        )
                    ],
                  ),
                ),
              ],
            ),
          );
        }
      },
    );
  }
}
