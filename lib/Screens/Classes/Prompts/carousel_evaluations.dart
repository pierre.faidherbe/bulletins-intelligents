import 'package:bulletins_intelligents_web_support/Cubits/Evaluations/evaluations_cubit.dart';
import 'package:bulletins_intelligents_web_support/Models/classe.dart';
import 'package:bulletins_intelligents_web_support/Models/eleve.dart';
import 'package:bulletins_intelligents_web_support/Models/evaluation.dart';
import 'package:bulletins_intelligents_web_support/Models/savoir.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/View/evaluer_view.dart';
import 'package:bulletins_intelligents_web_support/colors.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:intl/intl.dart';

class CarouselEvaluations extends StatefulWidget {
  const CarouselEvaluations({
    Key key,
    @required this.classe,
    @required this.eleves,
    @required this.date,
    @required this.savoir,
    @required this.showSnack,
  }) : super(key: key);

  final Classe classe;
  final Savoir savoir;
  final DateTime date;
  final List<Eleve> eleves;
  final VoidCallback showSnack;

  @override
  _CarouselEvaluationsState createState() => _CarouselEvaluationsState();
}

class _CarouselEvaluationsState extends State<CarouselEvaluations> {
  Color defaultColor = Colors.white;
  CarouselController carouselController = CarouselController();
  Map<int, Evaluation> evaluations = {};
  num cote;
  String commentaire;
  bool get allAcquisValides => evaluations.length == widget.eleves.length;

  void setEvaluations(Map<int, Evaluation> value) {
    setState(() {
      value.forEach((key, value) {
        evaluations[key] = value;
      });
    });

    print("list: $evaluations");
  }

  @override
  void initState() {
    print('eleves to evaluates ${widget.eleves}');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CarouselSlider.builder(
      carouselController: carouselController,
      itemCount: widget.eleves.length + 1,
      itemBuilder: (context, index) {
        return Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(35)),
          child: (index >= widget.eleves.length)
              ? ValidateCard(
                  allAcquisValides: allAcquisValides,
                  evaluations: evaluations,
                  widget: widget)
              : EvaluationCard(
                  //TODO: proposer élèves absents
                  evaluation: evaluations[index],
                  index: index,
                  date: widget.date,
                  eleve: widget.eleves[index],
                  savoir: widget.savoir,
                  carouselController: carouselController,
                  onEvaluationReceived: (value) => setEvaluations(value),
                ),
        );
      },
      options: CarouselOptions(
          scrollPhysics: NeverScrollableScrollPhysics(),
          enableInfiniteScroll: false,
          enlargeCenterPage: true,
          viewportFraction: 0.5,
          height: MediaQuery.of(context).size.height / 1.5),
    );
  }
}

class ValidateCard extends StatelessWidget {
  const ValidateCard({
    Key key,
    @required this.allAcquisValides,
    @required this.evaluations,
    @required this.widget,
  }) : super(key: key);

  final bool allAcquisValides;
  final Map<int, Evaluation> evaluations;
  final CarouselEvaluations widget;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: MediaQuery.of(context).size.width / 3,
            height: MediaQuery.of(context).size.height / 3,
            child: Material(
              type: MaterialType.circle,
              color: AppColors.getColor(0),
              child: InkWell(
                child: FlatButton(
                    onPressed: () {
                      evaluations.forEach((key, value) {
                        context.bloc<EvaluationsCubit>().addEvaluation(value);
                      });
                      Navigator.pop(context);
                      widget.showSnack();
                    },
                    child: Text(
                      'Valider',
                      style: TextStyle(color: Colors.white),
                    )),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class EvaluationCard extends StatefulWidget {
  const EvaluationCard({
    Key key,
    @required this.savoir,
    @required this.date,
    @required this.eleve,
    this.carouselController,
    @required this.index,
    this.evaluation,
    @required this.onEvaluationReceived,
  }) : super(key: key);

  final Savoir savoir;
  final DateTime date;
  final Eleve eleve;
  final Evaluation evaluation;
  final CarouselController carouselController;
  final int index;
  final ValueSetter<Map<int, Evaluation>> onEvaluationReceived;

  @override
  _EvaluationCardState createState() => _EvaluationCardState();
}

class _EvaluationCardState extends State<EvaluationCard> {
  final GlobalKey<FormBuilderState> fbKey = GlobalKey<FormBuilderState>();
  Evaluation evaluation;
  bool fieldEnable = true;

  void submitForm() {
    if (fbKey.currentState.saveAndValidate()) {
      print('form: ${fbKey.currentState.value}');
      setState(() {
        widget.eleve.classe.typeOfEvaluations == 'Cotes'
            ? evaluation = Evaluation(
                id: evaluation?.id ?? '',
                absent: fbKey.currentState.value['absent'],
                savoir: widget.savoir,
                eleve: widget.eleve,
                date: widget.date,
                commentaire: fbKey.currentState.value['commentaire'],
                cote: double.tryParse(
                  fbKey.currentState.value['cote'],
                ),
              )
            : evaluation = Evaluation(
                id: evaluation?.id ?? '',
                acquis: fbKey.currentState.value['evaluation'],
                savoir: widget.savoir,
                absent: fbKey.currentState.value['absent'],
                eleve: widget.eleve,
                date: widget.date,
                commentaire: fbKey.currentState.value['commentaire'],
              );
      });
      if (widget.carouselController != null) {
        widget.carouselController.animateToPage(widget.index + 1);
      }
      widget.onEvaluationReceived({widget.index: evaluation});
    }
  }

  @override
  void initState() {
    if (widget.evaluation != null) evaluation = widget.evaluation;
    print("card ${widget.evaluation}");
    super.initState();
  }

  Color getColor() {
    if (evaluation == null)
      return Colors.black;
    else if (evaluation.acquis == 'A.')
      return Colors.green;
    else if (evaluation.acquis == 'E.V.A.')
      return Colors.orange;
    else if (evaluation.acquis == 'N.A.')
      return Colors.red;
    else
      return null;
  }

  @override
  Widget build(BuildContext context) {
    return FormBuilder(
      key: fbKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Expanded(
                child: ListTile(
                  title: Text(
                    'Savoir: ${widget.savoir.name}',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: getColor()),
                  ),
                ),
              ),
              Expanded(
                child: ListTile(
                  title: Text(
                    'Elève: ${widget.eleve.fullName}',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: getColor()),
                  ),
                ),
              ),
              Expanded(
                child: ListTile(
                  title: Text(
                    'Date: ${DateFormat.yMMMMd().format(widget.date)}',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: getColor()),
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Flexible(flex: 1, child: Container()),
              widget.eleve.classe.typeOfEvaluations == 'Acquis'
                  ? Flexible(
                      flex: 4,
                      child: Column(
                        children: [
                          FormBuilderCustomField(
                            attribute: 'evaluation',
                            initialValue: evaluation?.acquis,
                            formField: FormField(
                              builder: (field) => InputDecorator(
                                decoration: InputDecoration(
                                    errorText: field.errorText,
                                    border: InputBorder.none),
                                child: RowCircleEvaluateButton(
                                  enabled: fieldEnable,
                                  field: field,
                                  index: widget.index,
                                  evaluation: evaluation,
                                ),
                              ),
                            ),
                          ),
                          FormBuilderCheckbox(
                            initialValue: false,
                            onChanged: (value) {
                              setState(() {
                                fieldEnable = !value;
                              });
                            },
                            decoration:
                                InputDecoration(border: InputBorder.none),
                            leadingInput: true,
                            attribute: 'absent',
                            label: Text('Absent ?'),
                          ),
                        ],
                      ),
                    )
                  : Container(),
              Flexible(flex: 1, child: Container()),
            ],
          ),
          Row(
            children: [
              Flexible(flex: 1, child: Container()),
              Flexible(
                flex: 4,
                child: widget.eleve.classe.typeOfEvaluations == 'Cotes'
                    ? Column(
                        children: [
                          FormBuilderTextField(
                            readOnly: !fieldEnable,
                            enabled: fieldEnable,
                            attribute: 'cote',
                            initialValue: evaluation?.cote?.toString(),
                            validators: [
                              FormBuilderValidators.min(0,
                                  errorText: 'la cote doit être positive'),
                              FormBuilderValidators.max(20,
                                  errorText:
                                      'la cote doit être plus petite que 20'),
                              FormBuilderValidators.numeric(
                                  errorText: 'La cote doit être un chiffre'),
                              //FormBuilderValidators.required(),
                            ],
                            decoration: InputDecoration(
                                labelText: "Cote", counterText: 'Cote sur /20'),
                          ),
                          FormBuilderCheckbox(
                            initialValue: false,
                            onChanged: (value) {
                              setState(() {
                                fieldEnable = !value;
                                fbKey.currentState.fields['cote'].currentState
                                    .reset();
                              });
                            },
                            decoration:
                                InputDecoration(border: InputBorder.none),
                            leadingInput: true,
                            attribute: 'absent',
                            label: Text('Absent ?'),
                            validators: [
                              (val) {
                                if (fbKey.currentState.fields['cote']
                                            .currentState.value ==
                                        '' &&
                                    fbKey.currentState.fields['absent']
                                            .currentState.value ==
                                        false) {
                                  return 'Vous devez rentrer une cote si l\'élève n\'est pas absent';
                                }
                              }
                            ],
                          ),
                        ],
                      )
                    : Container(),
              ),
              Flexible(flex: 1, child: Container()),
            ],
          ),
          Row(
            children: [
              Flexible(flex: 1, child: Container()),
              Flexible(
                flex: 4,
                child: FormBuilderTextField(
                  maxLines: 4,
                  attribute: 'commentaire',
                  initialValue: evaluation?.commentaire,
                  decoration: InputDecoration(
                    labelText: 'Commentaire  (optionnel)',
                  ),
                ),
              ),
              Flexible(flex: 1, child: Container()),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              widget.index != 0
                  ? IconButton(
                      icon: Icon(Icons.arrow_back),
                      onPressed: () => widget.carouselController.previousPage())
                  : Container(),
              FlatButton(onPressed: () => submitForm(), child: Text('Valider'))
            ],
          )
        ],
      ),
    );
  }
}
