import 'package:bulletins_intelligents_web_support/Cubits/Competences/competences_cubit.dart';
import 'package:bulletins_intelligents_web_support/Models/competence.dart';
import 'package:bulletins_intelligents_web_support/Models/matiere.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class AlertDialogCompetence extends StatefulWidget {
  const AlertDialogCompetence({Key key, this.matiere, this.competence})
      : super(key: key);
  final Matiere matiere;
  final Competence competence;

  @override
  _AlertDialogCompetenceState createState() => _AlertDialogCompetenceState();
}

class _AlertDialogCompetenceState extends State<AlertDialogCompetence> {
  final TextEditingController textEditingController = TextEditingController();
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  @override
  void initState() {
    super.initState();
    textEditingController.text = widget.competence?.name;
  }

  void addCompetence(Competence competence) {
    context.bloc<CompetencesCubit>().addCompetence(competence);
    Navigator.pop(context);
  }

  void deleteCompetence(Competence competence) {
    context.bloc<CompetencesCubit>().deleteCompetence(competence);
    Navigator.pop(context);
  }

  void updateCompetence(Competence competence) {
    context.bloc<CompetencesCubit>().updateCompetence(competence);
    Navigator.pop(context);
  }

  void submit() {
    if (_fbKey.currentState.saveAndValidate())
      widget.competence == null
          ? addCompetence(
              Competence(
                name: textEditingController.text,
                matiere: widget.matiere,
              ),
            )
          : updateCompetence(
              widget.competence.copyWith(
                name: textEditingController.text,
                //matiere: widget.matiere,
              ),
            );
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: widget.competence == null
          ? Text('Ajouter une compétence')
          : Text('Modifier la compétence'),
      content: FormBuilder(
        key: _fbKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            FormBuilderTextField(
              attribute: 'nom',
              
              controller: textEditingController,
              decoration: InputDecoration(labelText: 'Nom'),
              autofocus: true,
              validators: [
                FormBuilderValidators.minLength(1,
                    errorText: 'Entrez un nom pour la compétence')
              ],
              onFieldSubmitted: (value) => submit(),
            ),
          ],
        ),
      ),
      actions: [
        FlatButton(
          onPressed: () => Navigator.pop(context),
          child: Text('Annuler'),
        ),
        FlatButton(
          onPressed: () => submit(),
          child: widget.competence == null ? Text('Ajouter') : Text('Modifier'),
        ),
      ],
    );
  }
}
