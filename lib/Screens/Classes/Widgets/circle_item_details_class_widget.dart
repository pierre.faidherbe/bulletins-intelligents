import 'package:bulletins_intelligents_web_support/Models/classe.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/View/eleves_view.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/View/evaluer_view.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/View/menu_evaluations_view.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/View/menu_matieres_view.dart';
import 'package:bulletins_intelligents_web_support/Screens/Classes/View/evaluations_view.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CircleItemDetailsClasse extends StatelessWidget {
  const CircleItemDetailsClasse({
    Key key,
    @required this.title,
    @required this.routeIndex,
    @required this.classe,
    @required this.color,
  }) : super(key: key);

  final String title;
  final int routeIndex;
  final Classe classe;
  final Color color;

  @override
  Widget build(BuildContext context) {
    final views = {
      ElevesView.routeName: ElevesView(
        classe: classe,
      ),
      MenuMatieresView.routeName: MenuMatieresView(
        classe: classe,
      ),
      EvaluerView.routeName: EvaluerView(
        classe: classe,
      ),
      MenuEvaluationsView.routeName: MenuEvaluationsView(
        classe: classe,
      )
    };
    return Container(
      height: MediaQuery.of(context).size.height / 3,
      width: MediaQuery.of(context).size.width / 3,
      child: Material(
        color: color,
        type: MaterialType.circle,
        child: InkWell(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                settings: RouteSettings(name: views.keys.elementAt(routeIndex)),
                builder: (context) => views.values.elementAt(routeIndex),
              ),
            );
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ListTile(
                  title: Text(
                title,
                textAlign: TextAlign.center,
              ))
            ],
          ),
        ),
      ),
    );
  }
}
