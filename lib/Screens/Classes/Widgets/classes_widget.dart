import 'package:bulletins_intelligents_web_support/Cubits/Classes/classes_cubit.dart';
import 'package:bulletins_intelligents_web_support/Models/classe.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ClassesWidget extends StatefulWidget {
  const ClassesWidget({Key key, this.onClasseChange}) : super(key: key);

  final ValueChanged<Classe> onClasseChange;

  @override
  _ClassesWidgetState createState() => _ClassesWidgetState();
}

class _ClassesWidgetState extends State<ClassesWidget> {
  List<Classe> classes = [];
  Classe selectedClasse;
  bool sort = true;

  void addClasse(Classe classe) {
    context.bloc<ClassesCubit>().addClasse(classe);
    Navigator.pop(context);
  }

  void deleteClasse(Classe classe) {
    context.bloc<ClassesCubit>().deleteClasse(classe);
    Navigator.pop(context);
  }

  void updateClasse(Classe classe) {
    context.bloc<ClassesCubit>().updateClasse(classe);
    Navigator.pop(context);
  }

  onSortColum(int columnIndex, bool ascending) {
    if (columnIndex == 0) {
      if (ascending) {
        classes.sort((a, b) => a.name.compareTo(b.name));
      } else {
        classes.sort((a, b) => b.name.compareTo(a.name));
      }
    }
  }

  void _showDialog({Classe classe}) {
    TextEditingController textEditingController =
        TextEditingController(text: classe?.name);

    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: classe == null
            ? Text('Ajouter une classe')
            : Text('Modifier la classe'),
        content: TextField(
          controller: textEditingController,
          decoration: InputDecoration(labelText: 'Nom de la classe'),
        ),
        actions: [
          FlatButton(
            onPressed: () => Navigator.pop(context),
            child: Text('Annuler'),
          ),
          if (classe != null)
            FlatButton(
              onPressed: () => deleteClasse(classe),
              child: Text('Supprimer'),
              textColor: Colors.red,
            ),
          FlatButton(
            onPressed: () => classe == null
                ? addClasse(
                    Classe(name: textEditingController.text),
                  )
                : updateClasse(
                    classe.copyWith(name: textEditingController.text),
                  ),
            child: classe == null ? Text('Ajouter') : Text('Modifier'),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
          body: BlocBuilder<ClassesCubit, ClassesState>(
        builder: (context, state) {
          if (state is ClassesLoading) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is ClassesLoaded) {
            classes = state.classes;
            if (classes.isEmpty)
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width / 3,
                    child: Center(child: Text('Aucune classe crée')),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: IconButton(
                      icon: Icon(Icons.add),
                      onPressed: () => _showDialog(),
                      iconSize: 32,
                    ),
                  ),
                ],
              );
            return Container(
              width: MediaQuery.of(context).size.width / 3,
              child: Column(
                children: [
                  DataTable(
                    showCheckboxColumn: true,
                    sortAscending: sort,
                    sortColumnIndex: 0,
                    columns: [
                      DataColumn(
                        onSort: (columnIndex, ascending) {
                          setState(() {
                            sort = !sort;
                          });
                          onSortColum(columnIndex, ascending);
                        },
                        label: Text(
                          'Nom',
                          style: TextStyle(fontSize: 18),
                        ),
                      ),
                    ],
                    rows: classes
                        .map(
                          (e) => DataRow(
                              cells: [
                                DataCell(
                                  Text(e.name),
                                  onTap: () => _showDialog(classe: e),
                                ),
                              ],
                              onSelectChanged: (value) {
                                setState(() {
                                  selectedClasse = e;
                                });

                                widget.onClasseChange(selectedClasse);
                              },
                              selected: selectedClasse == e),
                        )
                        .toList(),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: IconButton(
                      icon: Icon(Icons.add),
                      onPressed: () => _showDialog(),
                      iconSize: 32,
                    ),
                  ),
                ],
              ),
            );
          }
        },
      ),
    );
  }
}
