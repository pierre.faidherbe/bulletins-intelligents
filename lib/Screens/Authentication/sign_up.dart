import 'package:bulletins_intelligents_web_support/Cubits/Sign_up/sign_up_cubit.dart';
import 'package:bulletins_intelligents_web_support/Repositories/authentication_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'sign_up_form.dart';

class SignUpPage extends StatelessWidget {
  const SignUpPage({Key key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => const SignUpPage());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Créer un compte')),
      body: Center(
        child: Container(
          alignment: Alignment.center,
          width: MediaQuery.of(context).size.width/3,
          child: BlocProvider<SignUpCubit>(
            create: (_) => SignUpCubit(
              context.repository<AuthenticationRepository>(),
            ),
            child: SignUpForm(),
          ),
        ),
      ),
    );
  }
}
