import 'package:bulletins_intelligents_web_support/Cubits/Login/login_cubit.dart';
import 'package:bulletins_intelligents_web_support/Repositories/authentication_repository.dart';
import 'package:bulletins_intelligents_web_support/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'login_form.dart';

class LoginPage extends StatelessWidget {
  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => LoginPage());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Bulletins Intelligents')),
      body: Container(
        decoration:
            BoxDecoration(color: AppColors.getColor(2), shape: BoxShape.circle),
        child: Center(
          child: Container(
            width:
                MediaQuery.of(context).size.width / 3,
            child: BlocProvider(
              create: (_) => LoginCubit(
                context.repository<AuthenticationRepository>(),
              ),
              child: LoginForm(),
            ),
          ),
        ),
      ),
    );
  }
}
