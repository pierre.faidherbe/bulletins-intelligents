import 'dart:html';

import 'package:bulletins_intelligents_web_support/Repositories/authentication_repository.dart';
import 'package:bulletins_intelligents_web_support/Repositories/competences_repository.dart';
import 'package:bulletins_intelligents_web_support/Repositories/matieres_repository.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import 'Cubits/Classes/classes_cubit.dart';
import 'Cubits/Competences/competences_cubit.dart';
import 'Cubits/Eleves/eleves_cubit.dart';
import 'Cubits/Evaluations/evaluations_cubit.dart';
import 'Cubits/Matieres/matieres_cubit.dart';
import 'Cubits/Savoirs/savoirs_cubit.dart';
import 'Repositories/evaluations_repository.dart';
import 'Repositories/savoirs_repository.dart';
import 'Repositories/classe_repository.dart';
import 'Repositories/eleve_repository.dart';
import 'Screens/Authentication/login_page.dart';
import 'Screens/Classes/View/menu_classes_view.dart';
import 'bloc/authentication_bloc.dart';
import 'splash.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  if (window.location.hostname == 'localhost') {
    FirebaseFirestore.instance.settings =
        Settings(host: 'localhost:8080', sslEnabled: false);
  }
  Intl.defaultLocale = 'fr';
  runApp(App(
    authenticationRepository: AuthenticationRepository(),
  ));
}

class App extends StatelessWidget {
  const App({Key key, @required this.authenticationRepository})
      : super(key: key);

  final AuthenticationRepository authenticationRepository;

  @override
  Widget build(BuildContext context) {
    return RepositoryProvider.value(
      value: authenticationRepository,
      child: BlocProvider(
        create: (_) => AuthenticationBloc(
          authenticationRepository: authenticationRepository,
        ),
        child: MyApp(),
      ),
    );
  }
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _navigatorKey = GlobalKey<NavigatorState>();

  NavigatorState get _navigator => _navigatorKey.currentState;

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<CompetencesCubit>(
          create: (context) => CompetencesCubit(
            competencesRepository: CompetencesRepository(),
          ),
        ),
        BlocProvider<MatieresCubit>(
          create: (context) =>
              MatieresCubit(matieresRepository: MatieresRepository()),
        ),
        BlocProvider<ClassesCubit>(
          create: (context) =>
              ClassesCubit(classesRepository: ClassesRepository()),
        ),
        BlocProvider<ElevesCubit>(
          create: (context) =>
              ElevesCubit(elevesRepository: ElevesRepository()),
        ),
        BlocProvider<SavoirsCubit>(
          create: (context) =>
              SavoirsCubit(savoirsRepository: SavoirsRepository()),
        ),
        BlocProvider<EvaluationsCubit>(
          create: (context) =>
              EvaluationsCubit(evaluationsRepository: EvaluationsRepository()),
        ),
      ],
      child: FeatureDiscovery(
        child: MaterialApp(
          title: 'Bulletins Intelligents',
          navigatorKey: _navigatorKey,
          locale: Locale('fr', 'FR'),
          localizationsDelegates: [
            GlobalMaterialLocalizations.delegate,
          ],
          theme: ThemeData(
              scaffoldBackgroundColor: Colors.white,
              appBarTheme: AppBarTheme(
                iconTheme: IconThemeData(color: Colors.black),
                elevation: 0,
                color: Colors.white,
                textTheme: TextTheme(
                  headline6: GoogleFonts.deliusSwashCaps(
                      color: Colors.black, fontSize: 30),
                ),
              ),
              textTheme: TextTheme(
                subtitle1: GoogleFonts.deliusSwashCaps(fontSize: 20),
              ),
              primarySwatch: Colors.blue,
              visualDensity: VisualDensity.adaptivePlatformDensity,
              buttonTheme: ButtonThemeData(
                buttonColor: Colors.blue,
                textTheme: ButtonTextTheme
                    .primary, //  <-- this auto selects the right color
              ),
              iconTheme: IconThemeData(color: Colors.blue)),
          builder: (context, child) {
            return BlocListener<AuthenticationBloc, AuthenticationState>(
              listener: (context, state) {
                switch (state.status) {
                  case AuthenticationStatus.authenticated:
                    _navigator.pushAndRemoveUntil<void>(
                      MenuClassesView.route(),
                      (route) => false,
                    );
                    break;
                  case AuthenticationStatus.unauthenticated:
                    _navigator.pushAndRemoveUntil<void>(
                      LoginPage.route(),
                      (route) => false,
                    );
                    break;
                  default:
                    break;
                }
              },
              child: child,
            );
          },
          //home: SplashPage(),
          onGenerateRoute: (_) => SplashPage.route(),
        ),
      ),
    );
  }
}

/* class HomePage extends StatefulWidget {
  const HomePage({
    Key key,
  }) : super(key: key);

  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => HomePage());
  }

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int currentIndex = 0;

  var views = [
    ClassesElevesView(),
    CompetencesMatieresView(),
    AcquisPage(),
    EvaluerPage(),
  ];

  List<NavigationRailDestination> destinations = [
    NavigationRailDestination(
      icon: Icon(Icons.group_outlined),
      label: Text('Classes / Elèves'),
      selectedIcon: Icon(Icons.group),
    ),
    NavigationRailDestination(
      icon: Icon(Icons.build_outlined),
      label: Text('Compétences / Matières'),
      selectedIcon: Icon(Icons.build),
    ),
    NavigationRailDestination(
      icon: Icon(Icons.star_border_outlined),
      label: Text('Acquis'),
      selectedIcon: Icon(Icons.star),
    ),
    NavigationRailDestination(
      icon: Icon(Icons.edit_outlined),
      label: Text('Evaluer'),
      selectedIcon: Icon(Icons.edit),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        NavigationRail(
          backgroundColor: Colors.white,
          extended: false,
          destinations: destinations,
          selectedIndex: currentIndex,
          onDestinationSelected: (int index) {
            setState(() {
              currentIndex = index;
            });
          },
          labelType: NavigationRailLabelType.selected,
        ),
        // This is the main content.
        Expanded(
          child: Center(child: views.elementAt(currentIndex)),
        ),
      ],
    );
  }
} */
