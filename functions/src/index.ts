import * as functions from "firebase-functions";
import admin = require("firebase-admin");

admin.initializeApp();

const db = admin.firestore();

exports.registerUser = functions.auth.user().onCreate((user) => {
  return db
    .collection("Users")
    .doc(user.uid)
    .set({ id: user.uid, name: user.displayName, sign_up_date: Date.now() });
});

//#region Matiere

exports.onUpdateMatiere = functions.firestore
  .document("Users/{User}/Matieres/{Matiere}")
  .onUpdate(async (change, context) => {
    const id = change.before.ref.parent.parent?.path;

    const competences = db
      .collection(`${id}/Competences`)
      .where("matiere.id", "==", change.after.id);

    return competences
      .get()
      .then((value) =>
        value.docs.forEach((x) =>
          x.ref.update({ matiere: change.after.data() })
        )
      );
  });

exports.onDeleteMatiere = functions.firestore
  .document("Users/{User}/Matieres/{Matiere}")
  .onDelete(async (snap, context) => {
    const id = snap.ref.parent.parent?.path;

    const competences = db
      .collection(`${id} / Competences`)
      .where("matiere.id", "==", snap.id);
    const savoirs = db
      .collection(`${id}/Savoirs`)
      .where("competence.matiere.id", "==", snap.id);
    const evaluations = db
      .collection(`${id}/Evaluations`)
      .where("savoir.competence.matiere.id", "==", snap.id);

    return competences
      .get()
      .then((value) => value.docs.forEach((x) => x.ref.delete()))
      .then((_) =>
        savoirs.get().then((value) => value.docs.forEach((x) => x.ref.delete()))
      )
      .then((__) =>
        evaluations
          .get()
          .then((value) => value.docs.forEach((x) => x.ref.delete()))
      );
  });

//#endregion

//#region Competences
exports.onUpdateCompetence = functions.firestore
  .document("Users/{User}/Competences/{Competence}")
  .onUpdate(async (change, context) => {
    const id = change.before.ref.parent.parent?.path;

    const savoirs = db
      .collection(`${id}/Savoirs`)
      .where("competence.id", "==", change.after.id);

    return savoirs
      .get()
      .then((value) =>
        value.docs.forEach((x) =>
          x.ref.update({ competence: change.after.data() })
        )
      );
  });

exports.onDeleteCompetence = functions.firestore
  .document("Users/{User}/Competences/{Competence}")
  .onDelete(async (snap, context) => {
    const id = snap.ref.parent.parent?.path;

    const savoirs = db
      .collection(`${id}/Savoirs`)
      .where("competence.id", "==", snap.id);
    const evaluations = db
      .collection(`${id}/Evaluations`)
      .where("savoir.competence.id", "==", snap.id);

    functions.logger.info("id:" + snap.id);

    return savoirs
      .get()
      .then((value) => value.docs.forEach((x) => x.ref.delete()))
      .then((__) =>
        evaluations
          .get()
          .then((value) => value.docs.forEach((x) => x.ref.delete()))
      );
  });

//#endregion

//#region Savoirs
exports.onUpdateSavoir = functions.firestore
  .document("Users/{User}/Savoirs/{Savoir}")
  .onUpdate(async (change, context) => {
    const id = change.before.ref.parent.parent?.path;

    const evaluations = db
      .collection(`${id}/Evaluations`)
      .where("savoir.id", "==", change.after.id);

    return evaluations
      .get()
      .then((value) =>
        value.docs.forEach((x) => x.ref.update({ savoir: change.after.data() }))
      );
  });

exports.onDeleteSavoir = functions.firestore
  .document("Users/{User}/Savoirs/{Savoir}")
  .onDelete(async (snap, context) => {
    const id = snap.ref.parent.parent?.path;

    const evaluations = db
      .collection(`${id}/Evaluations`)
      .where("savoir.id", "==", snap.id);

    return evaluations
      .get()
      .then((value) => value.docs.forEach((x) => x.ref.delete()));
  });

//#endregion

//#region Classe
exports.onUpdateClasse = functions.firestore
  .document("Users/{User}/Classes/{Classe}")
  .onUpdate(async (change, context) => {
    const id = change.before.ref.parent.parent?.path;

    const eleves = db
      .collection(`${id}/Eleves`)
      .where("classe.id", "==", change.after.id);
    const matieres = db
      .collection(`${id}/Matieres`)
      .where("classe.id", "==", change.after.id);

    functions.logger.info(id);

    return eleves
      .get()
      .then((value) =>
        value.docs.forEach((x) => x.ref.update({ classe: change.after.data() }))
      )
      .then((_) =>
        matieres
          .get()
          .then((value) =>
            value.docs.forEach((x) =>
              x.ref.update({ classe: change.after.data() })
            )
          )
      );
  });

exports.onDeleteClasse = functions.firestore
  .document("Users/{User}/Classes/{Classe}")
  .onDelete(async (snap, context) => {
    const id = snap.ref.parent.parent?.path;

    const eleves = db
      .collection(`${id}/Eleves`)
      .where("classe.id", "==", snap.id);

    return eleves
      .get()
      .then((value) => value.docs.forEach((x) => x.ref.delete()));
  });

exports.onClasseCreate = functions.firestore
  .document("Users/{User}/Classes/{Classe}")
  .onCreate(async (snap, context) => {
    const id = snap.ref.parent.parent?.path;

    return db
      .collection(`${id}/Matieres`)
      .add({ name: "Mathématiques", classe: snap.data(), id: "" })
      .then((x) => x.update({ id: x.id, "classe.id": snap.id }))
      .then((_) =>
        db
          .collection(`${id}/Matieres`)
          .add({ name: "Eveil", classe: snap.data(), id: "" })
          .then((x) => x.update({ id: x.id, "classe.id": snap.id }))
      )
      .then((_) =>
        db
          .collection(`${id}/Matieres`)
          .add({ name: "Français", classe: snap.data(), id: "" })
          .then((x) => x.update({ id: x.id, "classe.id": snap.id }))
      );
  });

//#endregion

exports.onDeleteEleve = functions.firestore
  .document("Users/{User}/Eleves/{Eleve}")
  .onDelete(async (snap, context) => {
    const id = snap.ref.parent.parent?.path;

    const eva = db
      .collection(`${id}/Evaluations`)
      .where("eleve.id", "==", snap.id);

    return eva.get().then((value) => value.docs.forEach((x) => x.ref.delete()));
  });

exports.onUpdateEleve = functions.firestore
  .document("Users/{User}/Eleves/{Eleve}")
  .onUpdate(async (change, context) => {
    const id = change.before.ref.parent.parent?.path;

    const eva = db
      .collection(`${id}/Evaluations`)
      .where("eleve.id", "==", change.after.id);

    return eva
      .get()
      .then((value) =>
        value.docs.forEach((x) => x.ref.update({ eleve: change.after.data() }))
      );
  });

exports.lauriane = functions.https.onRequest(async (req, res) => {
  /*    const savoirCalculer = await db
    .doc("/Users/GYKGDqQXU5glaSnYXEgnjSNJ5j52/Competences/W9F0vzqLhBRQKKWk7GU4")
    .get();

    const grammaire = await db
      .doc("/Users/GYKGDqQXU5glaSnYXEgnjSNJ5j52/Competences/uNLLQxvmEuSm0TsDJOE1")
      .get();

  //les nombres
   await db
    .doc("/Users/GYKGDqQXU5glaSnYXEgnjSNJ5j52/Savoirs/dzch4dbFFPhTMP8uwfxk")
    .update({ competence: savoirCalculer.data() }); 

  //ordre coirssant
  await db
    .doc("/Users/GYKGDqQXU5glaSnYXEgnjSNJ5j52/Savoirs/2QRO4BbBHue2TEqTkL6R")
    .update({ competence: savoirCalculer.data() }); 


  // l'ordre alpha
    await db
    .doc("/Users/GYKGDqQXU5glaSnYXEgnjSNJ5j52/Savoirs/o3UwONcAXiplyFhboeDS")
    .update({ competence: grammaire.data() }); */
});

exports.scheduledFirestoreExport = functions.pubsub
  .schedule("every monday 03:00")
  .timeZone("Europe/Brussels")
  .onRun((context) => {
    const client = new admin.firestore.v1.FirestoreAdminClient({});

    const bucket = "gs://bulletins-intelligents.appspot.com";

    const projectId = "bulletins-intelligents";

    const databaseName = client.databasePath(projectId, "(default)");

    return client
      .exportDocuments({
        name: databaseName,
        outputUriPrefix: bucket,
        // Leave collectionIds empty to export all collections
        // or set to a list of collection IDs to export,
        // collectionIds: ['users', 'posts']
        collectionIds: [],
      })
      .then((responses: any) => {
        const response = responses[0];
        console.log(`Operation Name: ${response["name"]}`);
      })
      .catch((err: any) => {
        console.error(err);
        throw new Error("Export operation failed");
      });
  });
